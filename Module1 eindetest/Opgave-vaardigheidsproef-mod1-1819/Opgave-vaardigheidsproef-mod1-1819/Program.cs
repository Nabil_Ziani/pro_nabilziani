﻿using System;

namespace Opgave_vaardigheidsproef_mod1_1819
{
    class Program
    {
        static void Main(string[] args)
        {
            string chefsExtra = "";

            //Bevraging
            Console.WriteLine("Welke Pizzabodem wenst u?");
            string keuzePizzabodem = Console.ReadLine();
            if (keuzePizzabodem == "martian meal")
            {
                Console.WriteLine("Martian meal is niet geschikt voor kinderen onder de 54 jaar, hoe oud bent u?");
                int leeftijd = Convert.ToInt32(Console.ReadLine());
                if (leeftijd < 54)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("ERROR");
                    Console.ResetColor();
                }
            }
            Console.WriteLine("Welke topping wenst u?");
            string keuzeTopping = Console.ReadLine();
            if (keuzeTopping == "geen")
            {
                Console.WriteLine("Wilt u de chef\'s extra?");
                chefsExtra = Console.ReadLine();
            }
            Console.WriteLine("Hoe ver is het afleveradres in lichtjaren?");
            int afstand = Convert.ToInt32(Console.ReadLine());
            if (afstand < 1 || afstand > 100)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("ERROR");
            }

            //Prijsbepaling van pizza
            double prijsPizzabodem = 0;
            if (keuzePizzabodem == "cheesy crust")
            {
                prijsPizzabodem = 5;
            }
            else if (keuzePizzabodem == "martian meal")
            {
                prijsPizzabodem = 2.8;
            }
            else if (keuzePizzabodem == "pegasus lime")
            {
                prijsPizzabodem = 12.4;
            }

            double prijsTopping = 0;
            if (keuzeTopping == "endrali pies" && keuzePizzabodem == "cheesy crust")
            {
                prijsTopping = 10;
            }
            else if (keuzeTopping == "endrali pies" && keuzePizzabodem == "martian meal" || keuzePizzabodem == "pegasus lime")
            {
                prijsTopping = 15;
            }
            else if (keuzeTopping == "italian cheese")
            {
                prijsTopping = 5.5;
            }
            else if (keuzeTopping == "geen")
            {
                if (chefsExtra == "ja")
                {
                    prijsTopping = 1;
                }
                else
                {
                    prijsTopping = 0;
                }
            }

            double pizzaPrijs = (prijsPizzabodem + prijsTopping);

            //Prijsbepaling transportkosten
            double transportKosten = 0;
            if (afstand < 10)
            {
                transportKosten = 25;
            }
            else if (afstand >= 10)
            {
                transportKosten = Math.Sqrt(afstand / pizzaPrijs) + pizzaPrijs;
                transportKosten = Math.Floor(transportKosten);
            }
            //Random korting-module
            if (chefsExtra == "ja")
            {
                Random rnd = new Random();
                int korting = rnd.Next(0, 51);
                transportKosten = transportKosten - ((double)korting / 100) * 10;
            }

            //Benzine module
            double aantalBenzineTonnen = ((double)afstand / 5);
            double laatsteTon = aantalBenzineTonnen - (int)aantalBenzineTonnen;
            double laatsteTonProcent;
            if (laatsteTon == 0)
            {
                laatsteTonProcent = 100;
            }
            else
            {
                laatsteTonProcent = laatsteTon * 100;
            }

            //Visualisatie
            Console.WriteLine("Je pizzabestelling is als volgt: ");
            if (keuzePizzabodem == "cheesy crust")
            {
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine("C");
                Console.ResetColor();
            }
            else if (keuzePizzabodem == "martian meal")
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("M");
                Console.ResetColor();
            }
            else
            {
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("P");
                Console.ResetColor();
            }
            if (keuzeTopping == "endrali pies")
            {
                Console.ForegroundColor = ConsoleColor.Blue;
                Console.WriteLine("O");
                Console.ResetColor();
            }
            else if (keuzeTopping == "italian cheese")
            {
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine("O");
                Console.ResetColor();
            }
            else if (keuzeTopping == "geen" && chefsExtra == "ja")
            {
                Console.ForegroundColor = ConsoleColor.White;
                Console.WriteLine("E");
                Console.ResetColor();
            }
            else if (keuzeTopping == "geen" && chefsExtra == "nee")
            {
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine("Z");
                Console.ResetColor();
            }

            //Ticket
            Console.WriteLine($"{keuzePizzabodem}:               {prijsPizzabodem} IC");
            Console.WriteLine($"{keuzeTopping}:             {prijsTopping} IC");
            Console.WriteLine("-   -   -   -   -   -   -   -   -   -     ");
            Console.WriteLine($"Totaal pizza:               {pizzaPrijs} IC");
            Console.WriteLine("");
            Console.WriteLine($"Afstand:                    {afstand} lichtjaren");
            Console.WriteLine($"Transportkosten:            {transportKosten} IC");
            Console.WriteLine("-   -   -   -   -   -   -   -   -   -     ");
            Console.WriteLine($"TOTAAL:                     {pizzaPrijs + transportKosten} IC");
            Console.WriteLine($"**************************************");
            Console.WriteLine("Informatie voor de piloot: ");
            Console.WriteLine($"Benzine tonnen in te laden:         {Math.Ceiling(aantalBenzineTonnen)}");
            Console.WriteLine($"Benzine nodig uit laatste ton:     {laatsteTonProcent}%");
            Console.ReadLine();
        }
    }
}
