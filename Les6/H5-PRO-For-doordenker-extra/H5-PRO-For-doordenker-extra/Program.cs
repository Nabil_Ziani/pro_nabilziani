﻿using System;

namespace H5_PRO_For_doordenker_extra
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Geef het aantal lijnen in: ");
            int lijnen = Convert.ToInt32(Console.ReadLine());
            int witRuimte = lijnen + 3;

            //De buitenste loop om het aantal lijnen uit te printen
            for (int i = 1; i <= lijnen; i++)
            {
                //De leegtes uitprinten
                for (int k = witRuimte; k >= 1; k--)
                {
                    Console.Write(" ");
                }
                //De sterren uitprinten, 
                for (int j = 1; j <= 2 * i - 1; j++)
                {
                    Console.Write("*");
                }
                //Om naar de volgende lijn te gaan
                Console.WriteLine();
                //Om te zorgen dat driehoek breder wordt (langs links), bij elke volgende lijn minder witruimte 
                witRuimte--;
            }
            Console.ReadLine();
        }
    }
}
