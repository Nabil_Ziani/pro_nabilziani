﻿using System;

namespace H5_PRO_Armstrong_nummer
{
    class Program
    {
        static void Main(string[] args)
        {
            double getalApart;
            double berekeningArmstrong = 0;

            Console.Write("Geef een getal in: ");
            string getal = Console.ReadLine();
            double lengteVast = getal.Length;   // Deze getal wordt doorheen de programma niet gewijzigd

            int lengte = getal.Length;
            double getalToInt = Convert.ToInt32(getal);

            for (int i = 0; i < lengte; lengte--)   // We gaan evenlang loopen als de lengte van het getal
            {
                // Met de volgende berekening verkrijgen we telkens het aparte getal van links naar rechts:
                getalApart = getalToInt / Math.Pow(10, lengte-1);   // Dit doen we om de eenheid telkens te verkrijgen  
                getalApart = Math.Floor(getalApart);    // Afronden
                getalToInt = getalToInt - (getalApart * Math.Pow(10, lengte-1));    // getalToInt aanpassen om de volgende eenheid te kunnen verkrijgen

                berekeningArmstrong += Math.Pow(getalApart, lengteVast);    /* We steken de som van het aparte getal, verheven tot de macht "lengte van het volledige getal" 
                                                                               steeds in de variabele "berekeningArmstrong" */
            }                                                                  

            if (berekeningArmstrong == int.Parse(getal))    // Als berekeningArmstrong dus gelijk is aan het volledige getal hebben we een Armstrong-nummer!
            {
                Console.WriteLine("Het ingegeven getal is een Armstrong-nummer.");
            }
            else
            {
                Console.WriteLine("Het ingegeven getal is géén Armstrong-nummer.");
            }
            Console.ReadLine();
        }
    }
}
