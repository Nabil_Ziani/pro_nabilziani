﻿using System;

namespace H5_Grootste_getal
{
    class Program
    {

        static void Main(string[] args)
        {
            int x = 0;
            int y = 0;
            int grootsteGetal;
            do
            {
                y += x;
                int getal = x;
                grootsteGetal = Math.Max(x, getal);
                Console.WriteLine("Voer gehele waarden in (32767=stop)");
                string instring = Console.ReadLine();
                x = Convert.ToInt32(instring);
            } while (x != 32767);

            Console.WriteLine($"De som is {y} en het grootste getal dat werd ingevoerd is {grootsteGetal}");
            Console.ReadLine();
        }
    }
}
