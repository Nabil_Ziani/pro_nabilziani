﻿using System;

namespace H5_Raad_het_getal
{
    class Program
    {
        static void Main(string[] args)
        {
            int getal;
            int poging;
            bool gevonden = false;
            Random rand = new Random();
            getal = rand.Next(0, 10);
            int ondergrens = 0;
            int bovengrens = 100;
            int aantalLoops = 0;

            while (!gevonden && aantalLoops < 6)        //We blijven 7x in deze loop zolang het getal niet is geraden   
            {
                Console.WriteLine($"Geef een getal tussen {ondergrens} en {bovengrens}");
                poging = int.Parse(Console.ReadLine());
                aantalLoops += 1;
                //Zolang ingegeven getal binnen interval is blijven we deze code herhalen (maximaal 7 keer)
                if (poging >= ondergrens && poging <= bovengrens && aantalLoops <= 6)
                {
                    if (getal > poging)
                    {
                        Console.WriteLine("Het gezochte getal is groter.");
                    }
                    else if (getal < poging)
                    {
                        Console.WriteLine("Het gezochte getal is kleiner.");
                    }
                    else
                    {
                        gevonden = true;
                    }
                    //Zolang dat het ingegeven getal groter is dan de random-getal is er controle, wanneer kleiner dan blijft de bovengrens op haar vorige waarde
                    if (poging > getal)
                    {
                        bovengrens = poging;
                    }
                    else
                    {
                        bovengrens += 0;
                    }
                }
                //Als het ingegeven getal buiten de interval ligt printen we dit uit: 
                else if (poging < ondergrens || poging > bovengrens && aantalLoops <= 6)
                {
                    Console.BackgroundColor = ConsoleColor.DarkRed;
                    Console.WriteLine("Gelieve een getal tussen het opgegeven interval in te geven!");
                    Console.ResetColor();
                }
            }
            //Als het getal correct is geraden komen we hierin:
            if (gevonden == true)
            {
                Console.BackgroundColor = ConsoleColor.DarkGreen;
                Console.WriteLine($"Gevonden! Het te zoeken getal was inderdaad {getal}. Je had er {aantalLoops} pogingen voor nodig.");
                Console.ReadLine();
            }
            //Als het getal niet is geraden na 7 keer komen we hierin:
            else
            {
                Console.BackgroundColor = ConsoleColor.Red;
                Console.WriteLine($"Je mag maximum 7x een poging doen, volgende keer beter!");
                Console.ReadLine();
            }
        }
    }
}
