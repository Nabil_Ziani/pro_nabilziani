﻿using System;

namespace H5_BeerSong
{
    class Program
    {
        static void Main(string[] args)
        {
            int getal1 = 99;
            int getal2 = 98;
            for (int i = 0; i < 5; i++)
            {
                Console.WriteLine($"{getal1} bottles of beer on the wall, {getal1} bottles of beer.");
                Console.WriteLine($"Take one down and pass it around, {getal2} bottles of beer on the wall.");
                if (i == 4)
                {
                    Console.WriteLine(". . . .");
                }
                Console.WriteLine();
                getal1 -= 1;
                getal2 -= 1;
            }

            getal1 = 4;
            getal2 = 3;
            for (int i = 0; i < 4; i++)
            {
                if (i < 3)
                {
                    Console.WriteLine($"{getal1} bottles of beer on the wall, {getal1} bottles of beer.");
                    Console.WriteLine($"Take one down and pass it around, {getal2} bottles of beer on the wall.");
                    Console.WriteLine();
                }
                else
                {
                    Console.WriteLine($"{getal1} bottle of beer on the wall, {getal1} bottle of beer.");
                    Console.WriteLine($"Take it down and pass it around, no more bottles of beer on the wall.");
                    Console.WriteLine();
                    Console.WriteLine("No more bottles of beer on the wall, no more bottles of beer.");
                    Console.WriteLine("Go to the store and buy some more, 99 bottles of beer on the wall.");
                }
                getal1 -= 1;
                getal2 -= 1;
            }
            Console.ReadLine();
        }
    }
}
