﻿using System;

namespace H5_Boekhouder
{
    class Program
    {
        static void Main(string[] args)
        {
            int ingegevenGetal;
            int ingegevenGetalOpgeteld = 0;
            int negatieveGetallen = 0;
            int positieveGetallen = 0;
            double aantalLoops = 0;         //Dit is double omdat de gemiddelde een kommagetal kan zijn 
            do
            {
                Console.Write("Voer willekeurige waarden in (stop = 1111): ");
                string inputString = Console.ReadLine();
                ingegevenGetal = Convert.ToInt32(inputString);

                if (ingegevenGetal != 1111)         //Ik steek alles in if-statement om ervoor te zorgen dat zodra gebruiker 1111 ingeeft de programma uit loop gaat
                {
                    ingegevenGetalOpgeteld += ingegevenGetal;
                    aantalLoops += 1;           //Om het aantal getallen te weten
                    if (ingegevenGetal < 0)
                    {
                        negatieveGetallen += ingegevenGetal;
                    }
                    else
                    {
                        positieveGetallen += ingegevenGetal;
                    }

                    //De balans van alle ingevoerde getallen
                    Console.WriteLine($"De som van alle ingegeven getallen is {ingegevenGetalOpgeteld}.");
                    //De som van alle negatieve invoeren
                    Console.WriteLine($"De som van alle negatieve getallen is {negatieveGetallen}.");
                    //De som van alle positieve invoeren
                    Console.WriteLine($"De som van alle positieve getallen is {positieveGetallen}.");
                    //Het gemiddelde van alle ingevoerde getallen
                    Console.WriteLine($"Het gemiddelde van alle ingevoerde getallen is {Math.Round((ingegevenGetalOpgeteld / aantalLoops), 2)}");
                    Console.ReadLine();
                    Console.Clear();
                }
            } while (ingegevenGetal != 1111);
        }
    }
}
