﻿using System;

namespace H5_PRO_Password_generator
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Geef je familienaam in, begin met een hoofdletter: ");
            string familienaam = Console.ReadLine();
            Console.Write("Geef het zonenummer van je telefoonnummer in: ");
            string zoneNummer = Console.ReadLine();
            Console.Write("Geef je postcode in: ");
            string postcode = Console.ReadLine();

            //Paswoord genereren
            char eersteLetterFamilienaam = familienaam[0];
            char tweedeLetterFamilienaam = familienaam[1];
            char zoneNummerZonderNul = zoneNummer[1];
            int eersteCijferPostcode = int.Parse(postcode[0].ToString());
            
            Console.Write("Het gegenereerde paswoord is: ");
            Console.Write($"{tweedeLetterFamilienaam}{eersteLetterFamilienaam}{zoneNummerZonderNul}{Math.Pow(eersteCijferPostcode, 2)}");
            Console.ReadLine();
        }
    }
}
