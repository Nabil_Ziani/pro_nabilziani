﻿using System;

namespace H5_Tafels_van_vermenigvuldigen
{
    class Program
    {
        static void Main(string[] args)
        {
            int product;
            for (int i = 1; i <= 10; i++)
            {
                for (int j = 1; j <= 10; j++)
                {
                    product = i * j;
                    Console.WriteLine($"{i} x {j} = {product}");
                }
            }
            Console.ReadLine();
        }
    }
}
