﻿using System;

namespace H5_Become_Neo
{
    class Program
    {
        static void Main(string[] args)
        {
            Random rangen = new Random();
            Console.ForegroundColor = ConsoleColor.Green;
            while (true)
            {
                //Genereer nieuw random teken:
                char teken = Convert.ToChar(rangen.Next(62, 400));
                //Zet teken op scherm
                Console.Write(teken);

                //Ietwat vertragen
                System.Threading.Thread.Sleep(10); //dit getal is in milliseconden. Speel er gerust mee.

                //Random-kleurtjes
                if (rangen.Next(0, 3) == 0)
                {
                    Console.ForegroundColor = ConsoleColor.DarkGreen;
                }
                if (rangen.Next(3, 6) == 3)
                {
                    Console.ForegroundColor = ConsoleColor.DarkMagenta;
                }
                if (rangen.Next(6, 9) == 6)
                {
                    Console.ForegroundColor = ConsoleColor.DarkBlue;
                }
                if (rangen.Next(9, 12) == 9)
                {
                    Console.ForegroundColor = ConsoleColor.DarkCyan;
                }
                if (rangen.Next(12, 15) == 12)
                {
                    Console.ForegroundColor = ConsoleColor.DarkGray;
                }
                if (rangen.Next(15, 18) == 15)
                {
                    Console.ForegroundColor = ConsoleColor.DarkYellow;
                }
                if (rangen.Next(18, 21) == 18)
                {
                    Console.ForegroundColor = ConsoleColor.DarkRed;
                }
                if (rangen.Next(21, 24) == 21)
                {
                    Console.ForegroundColor = ConsoleColor.Green;
                }
            }
        }
    }
}
