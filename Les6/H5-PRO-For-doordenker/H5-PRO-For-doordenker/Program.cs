﻿using System;

namespace H5_PRO_For_doordenker
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Geef een maximum-waarde in: ");
            int maximumBreedte = Convert.ToInt32(Console.ReadLine());

            //De loop om het aantal lijnen (tot maximumwaarde) uit te printen
            for (int i = 1; i <= maximumBreedte; i++)
            {
                //De sterren uitprinten, 
                for (int j = 1; j <= i; j++)
                {
                    Console.Write("*");
                }
                //Om naar de volgende lijn te gaan
                Console.WriteLine();
            }
            //De loop om het aantal lijnen (na maximumwaarde) uit te printen
            for (int i = 0; i < maximumBreedte; i++)
            {
                //De sterren uitprinten, 
                for (int j = (maximumBreedte - 1); j > i; j--)
                {
                    Console.Write("*");
                }
                //Om naar de volgende lijn te gaan
                Console.WriteLine();
            }
            Console.ReadLine();
        }
    }
}
