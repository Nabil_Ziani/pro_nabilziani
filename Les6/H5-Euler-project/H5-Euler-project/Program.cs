﻿using System;

namespace H5_Euler_project
{
    class Program
    {
        static void Main(string[] args)
        {
            int somVeelvouden = 0;

            for (int i = 0; i <= 1000; i++)
            {
                if (i % 3 == 0 || i % 5 == 0)
                {
                    somVeelvouden += i;
                }
            }
            Console.WriteLine($"De som van alle veelvouden van 3 of 5 (onder 1000) is: {somVeelvouden}");
            Console.ReadLine();
        }
    }
}
