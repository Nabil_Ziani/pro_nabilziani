﻿using System;

namespace H5_Steen_schaar_papier
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] mogelijkeKeuzesComputer = { "steen", "papier", "schaar" };
            int scoreGebruiker = 0;
            int scoreComputer = 0;

            while (scoreComputer < 10 && scoreGebruiker < 10)
            {
                Console.WriteLine("Geef je keuze in: ");
                string keuzeGebruiker = Console.ReadLine();
                string keuzeComputer = RandomIndex(mogelijkeKeuzesComputer);
                Console.WriteLine($"Computer: {keuzeComputer}");

                if (keuzeGebruiker == "schaar")
                {
                    if (keuzeComputer == "schaar")
                    {
                        scoreGebruiker += 0;
                    }
                    else if (keuzeComputer == "papier")
                    {
                        scoreGebruiker += 1;
                        Console.ForegroundColor = ConsoleColor.Green;
                        Console.WriteLine("Gebruiker heeft deze ronde gewonnen!");
                        Console.ResetColor();
                    }
                    else if (keuzeComputer == "steen")
                    {
                        scoreGebruiker += 0;
                    }
                }
                if (keuzeGebruiker == "papier")
                {
                    if (keuzeComputer == "papier")
                    {
                        scoreGebruiker += 0;
                    }
                    else if (keuzeComputer == "steen")
                    {
                        scoreGebruiker += 1;
                        Console.ForegroundColor = ConsoleColor.Green;
                        Console.WriteLine("Gebruiker heeft deze ronde gewonnen!");
                        Console.ResetColor();
                    }
                    else if (keuzeComputer == "schaar")
                    {
                        scoreGebruiker += 0;
                    }
                }
                if (keuzeGebruiker == "steen")
                {
                    if (keuzeComputer == "papier")
                    {
                        scoreGebruiker += 0;
                    }
                    else if (keuzeComputer == "schaar")
                    {
                        scoreGebruiker += 1;
                        Console.ForegroundColor = ConsoleColor.Green;
                        Console.WriteLine("Gebruiker heeft deze ronde gewonnen!");
                        Console.ResetColor();
                    }
                    else if (keuzeComputer == "steen")
                    {
                        scoreGebruiker += 0;
                    }
                }

                if (keuzeComputer == "schaar")
                {
                    if (keuzeGebruiker == "schaar")
                    {
                        scoreComputer += 0;
                    }
                    else if (keuzeGebruiker == "papier")
                    {
                        scoreComputer += 1;
                        Console.ForegroundColor = ConsoleColor.Green;
                        Console.WriteLine("Computer heeft deze ronde gewonnen!");
                        Console.ResetColor();
                    }
                    else if (keuzeGebruiker == "steen")
                    {
                        scoreComputer += 0;
                    }
                }
                if (keuzeComputer == "papier")
                {
                    if (keuzeGebruiker == "papier")
                    {
                        scoreComputer += 0;
                    }
                    else if (keuzeGebruiker == "steen")
                    {
                        scoreComputer += 1;
                        Console.ForegroundColor = ConsoleColor.Green;
                        Console.WriteLine("Computer heeft deze ronde gewonnen!");
                        Console.ResetColor();
                    }
                    else if (keuzeGebruiker == "schaar")
                    {
                        scoreComputer += 0;
                    }
                }
                if (keuzeComputer == "steen")
                {
                    if (keuzeGebruiker == "papier")
                    {
                        scoreComputer += 0;
                    }
                    else if (keuzeGebruiker == "schaar")
                    {
                        scoreComputer += 1;
                        Console.ForegroundColor = ConsoleColor.Green;
                        Console.WriteLine("Computer heeft deze ronde gewonnen!");
                        Console.ResetColor();
                    }
                    else if (keuzeGebruiker == "steen")
                    {
                        scoreComputer += 0;
                    }
                }

                Console.WriteLine($"Huidige score gebruiker: {scoreGebruiker}");
                Console.WriteLine($"Huidige score computer: {scoreComputer}");
            }
            if (scoreComputer == 10)
            {
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine($"De winnaar is Computer!");
            }
            else
            {
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine($"De winnaar is Gebruiker!");
            }
            Console.ReadLine();
        }
        static string RandomIndex(string[] input)
        {
            Random rnd = new Random();
            int index = rnd.Next(input.Length);
            return input[index];
        }
    }
}
