﻿using System;

namespace Oefening_15
{
    class Program
    {
        static void Main(string[] args)
        {
            //Toon alle priemgetallen van 1 tot n 
            Console.Write("Geef een getal in: ");
            int ingegevenGetal = int.Parse(Console.ReadLine());
            int beginGetal = 1;
            bool priemGetal = true;

            //We blijven in deze loop zolang het ingegeven getal groter is dan 1
            for (int i = ingegevenGetal; i > beginGetal; i--)
            {
                int deelGetal = i;
                //In deze loop gaan we de huidige "ingegeven getal" (na elke loop doen we -1) door alle getallen eronder delen, behalve 0!
                for (int j = deelGetal; j > 1; j--)
                {
                    int rest = i % deelGetal;
                    //Als het ingegeven getal niet gelijk is aan zichzelf en toch een rest 0 heeft dan is het false --> géén priemgetal
                    if (i != deelGetal && rest == 0)
                    {
                        priemGetal = false;
                    }
                    deelGetal -= 1;
                }
                //Ik hoef niet te zeggen = true, dit is standaard al het geval
                if (priemGetal)
                {
                    Console.Write($"{i} ");
                }
                //Na elke loop de bool terug op true zetten 
                priemGetal = true;
            }
            Console.ReadLine();
        }
    }
}
