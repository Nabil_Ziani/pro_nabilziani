﻿using System;

namespace Oefening_1
{
    class Program
    {
        static void Main(string[] args)
        {
            //Toon alle natuurlijke getallen van 1 tot n.
            Console.WriteLine("Geef een getal in: ");
            int inputGetal = int.Parse(Console.ReadLine());
            int beginGetal = 0;
            do
            {
                beginGetal += 1;
                Console.Write($"{beginGetal} ");
            } while (beginGetal != inputGetal);
            Console.ReadLine();
        }
    }
}
