﻿using System;

namespace Oefening_8
{
    class Program
    {
        static void Main(string[] args)
        {
            //Schrijf een programma dat het aantal digits in een getal telt (.Length gebruiken dus)
            Console.Write("Geef een getal in: ");
            string inputGetal = Console.ReadLine();
            int aantalDigits = inputGetal.Length;
            Console.WriteLine($"Het ingegeven getal heeft {aantalDigits} digits.");
            Console.ReadLine();
        }
    }
}
