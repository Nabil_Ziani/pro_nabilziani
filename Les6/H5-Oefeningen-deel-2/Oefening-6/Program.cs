﻿using System;

namespace Oefening_6
{
    class Program
    {
        static void Main(string[] args)
        {
            //Toon de som van alle even getallen van 1 tot n 
            Console.Write("Geef een getal in: ");
            int inputGetal = int.Parse(Console.ReadLine());
            int getal = 0;
            int som = 0;

            while (getal < inputGetal - 1)
            {
                getal += 2;
                som += getal;
                Console.Write($"{getal} ");
            }

            Console.WriteLine($"\nDe som van deze getallen is {som}");
            Console.ReadLine();
        }
    }
}
