﻿using System;

namespace Oefening_12
{
    class Program
    {
        static void Main(string[] args)
        {
            //Schrijf een programma dat de macht van een getal toont. De gebruiker voert eerst het getal in,
            //gevolgd door de macht (bv. 2 en 4 zal als resultaat 16 geven)
            Console.Write("Geef het grondgetal in: ");
            int grondGetal = int.Parse(Console.ReadLine());
            Console.Write("Geef de exponent in: ");
            int exponent = int.Parse(Console.ReadLine());

            double resultaat = Math.Pow(grondGetal, exponent);
            Console.WriteLine($"De macht is {resultaat}.");
            Console.ReadLine();
        }
    }
}
