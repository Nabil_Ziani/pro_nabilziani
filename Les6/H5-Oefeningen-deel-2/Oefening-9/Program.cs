﻿using System;

namespace Oefening_9
{
    class Program
    {
        static void Main(string[] args)
        {
            //(PRO) Schrijf een programma dat een ingevoerd getal als tekst uitschrijft.
            //Als de gebruiker dus 123 invoert zal de uitvoer zijn: honderd drie en twintig.
            Console.Write("Geef een getal in: ");
            string inputGetal = Console.ReadLine();

            if (inputGetal.Length == 1)
            {
                Console.WriteLine(Eenheden(inputGetal));
            }
            else if (inputGetal.Length == 2)
            {
                Console.WriteLine(Tientallen(inputGetal));
            }
            else if (inputGetal.Length == 3)
            {
                Console.WriteLine(Honderdtallen(inputGetal));
            }
            Console.ReadLine();
        }
        private static string Eenheden(string inputGetal)
        {
            int inputGetalToInt = Convert.ToInt32(inputGetal);
            string getalInTekst = "";

            switch (inputGetalToInt)
            {
                case 1:
                    getalInTekst = "één";
                    break;
                case 2:
                    getalInTekst = "twee";
                    break;
                case 3:
                    getalInTekst = "drie";
                    break;
                case 4:
                    getalInTekst = "vier";
                    break;
                case 5:
                    getalInTekst = "vijf";
                    break;
                case 6:
                    getalInTekst = "zes";
                    break;
                case 7:
                    getalInTekst = "zeven";
                    break;
                case 8:
                    getalInTekst = "acht";
                    break;
                case 9:
                    getalInTekst = "negen";
                    break;
            }
            return getalInTekst;
        }
        private static string Tientallen(string inputGetal)
        {
            int inputGetalToInt = Convert.ToInt32(inputGetal);
            string getalInTekst = "";

            switch (inputGetalToInt)
            {
                case 10:
                    getalInTekst = "tien";
                    break;
                case 11:
                    getalInTekst = "elf";
                    break;
                case 12:
                    getalInTekst = "twaalf";
                    break;
                case 13:
                    getalInTekst = "dertien";
                    break;
                case 14:
                    getalInTekst = "veertien";
                    break;
                case 15:
                    getalInTekst = "vijftien";
                    break;
                case 16:
                    getalInTekst = "zestien";
                    break;
                case 17:
                    getalInTekst = "zeventien";
                    break;
                case 18:
                    getalInTekst = "achtien";
                    break;
                case 19:
                    getalInTekst = "negentien";
                    break;
                case 20:
                    getalInTekst = "twintig";
                    break;
                case 30:
                    getalInTekst = "dertig";
                    break;
                case 40:
                    getalInTekst = "veertig";
                    break;
                case 50:
                    getalInTekst = "vijftig";
                    break;
                case 60:
                    getalInTekst = "zestig";
                    break;
                case 70:
                    getalInTekst = "zeventig";
                    break;
                case 80:
                    getalInTekst = "tachtig";
                    break;
                case 90:
                    getalInTekst = "negentig";
                    break;
                default:
                    if (inputGetalToInt > 0)
                    {
                        getalInTekst = Eenheden(inputGetal.Substring(1)) + " en " + Tientallen(inputGetal.Substring(0, 1) + "0");
                    }
                    break;
            }
            return getalInTekst;
        }
        private static string Honderdtallen(string inputGetal)
        {
            int inputGetalToInt = Convert.ToInt32(inputGetal);
            string getalInTekst = "";

            switch (inputGetalToInt)
            {
                case 100:
                    getalInTekst = "honderd";
                    break;
                case 101:
                    getalInTekst = "honderd en één";
                    break;
                case 102:
                    getalInTekst = "honderd en twee";
                    break;
                case 103:
                    getalInTekst = "honderd en drie";
                    break;
                case 104:
                    getalInTekst = "honderd en vier";
                    break;
                case 105:
                    getalInTekst = "honderd en vijf";
                    break;
                case 106:
                    getalInTekst = "honderd en zes";
                    break;
                case 107:
                    getalInTekst = "honderd en zeven";
                    break;
                case 108:
                    getalInTekst = "honderd en acht";
                    break;
                case 109:
                    getalInTekst = "honderd en negen";
                    break;
                case 200:
                    getalInTekst = "tweehonderd";
                    break;
                case 300:
                    getalInTekst = "driehonderd";
                    break;
                case 400:
                    getalInTekst = "vierhonderd";
                    break;
                case 500:
                    getalInTekst = "vijfhonderd";
                    break;
                case 600:
                    getalInTekst = "zeshonderd";
                    break;
                case 700:
                    getalInTekst = "zevenhonderd";
                    break;
                case 800:
                    getalInTekst = "achthonderd";
                    break;
                case 900:
                    getalInTekst = "negenhonderd";
                    break;
                default:
                    if (inputGetalToInt > 0)
                    {
                        if (inputGetal.Substring(2, 1) == "0")
                        {
                            getalInTekst = Honderdtallen(inputGetal.Substring(0, 1) + "00") + Tientallen(inputGetal.Substring(1, 1) + "0");
                        }
                        else
                        {
                            getalInTekst = Honderdtallen(inputGetal.Substring(0, 1) + "00") + " " + Eenheden(inputGetal.Substring(2, 1)) + " en " + Tientallen(inputGetal.Substring(1, 1) + "0");
                        }
                    }
                    break;
            }
            return getalInTekst;
        }
    }
}
