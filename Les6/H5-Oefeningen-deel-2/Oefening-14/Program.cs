﻿using System;

namespace Oefening_14
{
    class Program
    {
        static void Main(string[] args)
        {
            //Schrijf een programma dat controleert of een getal priem is of niet
            Console.Write("Geef een getal in: ");
            int inputGetal = int.Parse(Console.ReadLine());
            int deelGetal = inputGetal;
            bool priemGetal = true;

            do
            {
                int rest;
                rest = inputGetal % deelGetal;
                //Als een getal deelbaar is door een ander getal dan zichzelf --> géén priemgetal
                if (deelGetal != inputGetal && rest == 0)
                {
                    priemGetal = false;
                }
                //Als het ingegeven getal één is --> géén priemgetal
                if (inputGetal == 1)
                {
                    priemGetal = false;
                }
                deelGetal -= 1;
            } while (deelGetal != 1 && deelGetal != 0);

            //De bool priemGetal is default true, dus als het niet false is printen we dit uit
            if (priemGetal == true)
            {
                Console.WriteLine($"{inputGetal} is een priemgetal");
            }
            //Als de bool false is, dan printen we dit uit
            else if (priemGetal == false)
            {
                Console.WriteLine($"{inputGetal} is géén priemgetal");
            }
            Console.ReadLine();
        }
    }
}
