﻿using System;

namespace Oefening_16
{
    class Program
    {
        static void Main(string[] args)
        {
            //Toon de reeks van Fibonacci tot n termen
            Console.Write("Geef een getal in: ");
            int ingegevenGetal = Convert.ToInt32(Console.ReadLine());
            int beginGetal = 0;
            int volgendeGetal = 1;
            int fibonacci;

            Console.Write("1 ");
            do
            {
                fibonacci = beginGetal + volgendeGetal;

                beginGetal = volgendeGetal;
                volgendeGetal = fibonacci;

                Console.Write($"{fibonacci} ");
            } while (fibonacci < ingegevenGetal);
            Console.ReadLine();
        }
    }
}
