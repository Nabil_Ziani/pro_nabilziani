﻿using System;

namespace Oefening_4
{
    class Program
    {
        static void Main(string[] args)
        {
            //Toon alle oneven getallen tussen 1 en 100
            int getal = 1;

            while (getal != 101)
            {
                Console.Write($"{getal} ");
                getal += 2;
            }
            Console.ReadLine();
        }
    }
}
