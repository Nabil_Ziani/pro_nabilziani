﻿using System;

namespace Oefening_13
{
    class Program
    {
        static void Main(string[] args)
        {
            //Schrijf een programma dat een getal n ontbindt in factoren. Factoren zijn de getallen 
            //waardoor je n kan delen zonder rest (van bijvoorbeeld het getal 100 zijn de factoren 1, 2, 4, 5, 10, 20, 25, 50, 100).
            Console.Write("Geef een getal in: ");
            int inputGetal = int.Parse(Console.ReadLine());
            int getal = inputGetal;

            while (getal >= 1)
            {
                int rest;
                rest = (inputGetal % getal);

                if (rest == 0)
                {
                    Console.Write($"{getal} ");
                }
                getal -= 1;
            }
            Console.ReadLine();
        }
    }
}
