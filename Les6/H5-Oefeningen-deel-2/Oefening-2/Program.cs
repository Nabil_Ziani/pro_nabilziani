﻿using System;

namespace Oefening_2
{
    class Program
    {
        static void Main(string[] args)
        {
            //Toon alle natuurlijke getallen van n tot 1.
            Console.WriteLine("Geef een maximum-getal in: ");
            int inputGetal = int.Parse(Console.ReadLine());
            while (inputGetal != 0)
            {
                Console.Write($"{inputGetal} ");
                inputGetal -= 1;
            }
            Console.ReadLine();
        }
    }
}
