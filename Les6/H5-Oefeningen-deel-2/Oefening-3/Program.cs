﻿using System;

namespace Oefening_3
{
    class Program
    {
        static void Main(string[] args)
        {
            //Toon alle even getallen tussen 1 en 100
            int getal = 0;

            while (getal != 100)
            {
                getal += 2;
                Console.Write($"{getal} ");
            }
            Console.ReadLine();
        }
    }
}
