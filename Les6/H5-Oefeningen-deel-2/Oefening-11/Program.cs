﻿using System;

namespace Oefening_11
{
    class Program
    {
        static void Main(string[] args)
        {
            //Toon het alfabet van a tot z
            char a = 'a';
            char z = 'z';

            while (a <= z)
            {
                Console.Write($"{a} ");
                a += (char)1;
            }
            Console.ReadLine();
        }
    }
}
