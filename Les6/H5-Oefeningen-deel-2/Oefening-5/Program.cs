﻿using System;

namespace Oefening_5
{
    class Program
    {
        static void Main(string[] args)
        {
            //Toon de som van alle getallen van 1 tot n
            Console.Write("Geef een getal in: ");
            int inputGetal = int.Parse(Console.ReadLine());
            int beginGetal = 0;
            int som = 0;
            do
            {
                beginGetal += 1;
                som += beginGetal;
                Console.Write($"{beginGetal} ");
            } while (beginGetal != inputGetal);
            Console.WriteLine($"\nDe som van deze getallen is {som}");
            Console.ReadLine();
        }
    }
}
