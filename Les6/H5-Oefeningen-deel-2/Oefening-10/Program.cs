﻿using System;

namespace Oefening_10
{
    class Program
    {
        static void Main(string[] args)
        {
            //Schrijf een programma dat alle ascii karakters en hun waarde toont van 10 tot n
            //(tip: char c = Convert.ToChar(65); zal hoofdletter A tonen) 
            Console.Write("Geef een getal in: ");
            int inputGetal = int.Parse(Console.ReadLine());
            int getalTien = 10;
            do
            {
                char c = Convert.ToChar(getalTien);
                getalTien += 1;
                Console.Write($"{c} ");
            } while (getalTien != inputGetal);
            Console.ReadLine();
        }
    }
}
