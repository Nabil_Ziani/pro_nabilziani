﻿using System;

namespace H5_RNA_Transscriptie
{
    class Program
    {
        static void Main(string[] args)
        {
            //gegeven DNA-string omzetten naar een RNA-string
            string letter;
            string volledigeInput = "";
            string inputOmgezet = "";

            for (int i = 0; i < 12; i++)
            {
                Console.Write("Voer 1 DNA-nucleotide in per keer en duw op enter (schrijf met hoofdletters): ");
                letter = Console.ReadLine();
                volledigeInput += letter;

                if (letter == "G")
                {
                    inputOmgezet += "C";
                }
                if (letter == "C")
                {
                    inputOmgezet += "G";
                }
                if (letter == "T")
                {
                    inputOmgezet += "A";
                }
                if (letter == "A")
                {
                    inputOmgezet += "U";
                }
            }

            Console.WriteLine($"DNA-complement = {volledigeInput}");
            Console.WriteLine($"RNA-complement = {inputOmgezet}");
            Console.ReadLine();
        }
    }
}
