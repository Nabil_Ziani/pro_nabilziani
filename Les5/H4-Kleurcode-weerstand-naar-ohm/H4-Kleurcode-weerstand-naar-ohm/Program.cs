﻿using System;

namespace H4_Kleurcode_weerstand_naar_ohm
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Geef de kleur van de eerste ring in:");
            string input1 = Console.ReadLine();
            int input1ToInt = 0;
            switch (input1)
            {
                case "zwart":
                    input1ToInt = 0;
                    break;
                case "bruin":
                    input1ToInt = 1;
                    break;
                case "rood":
                    input1ToInt = 2;
                    break;
                case "oranje":
                    input1ToInt = 3;
                    break;
                case "geel":
                    input1ToInt = 4;
                    break;
                case "groen":
                    input1ToInt = 5;
                    break;
                case "blauw":
                    input1ToInt = 6;
                    break;
                case "paars":
                    input1ToInt = 7;
                    break;
                case "grijs":
                    input1ToInt = 8;
                    break;
                case "wit":
                    input1ToInt = 9;
                    break;
            }
            Console.WriteLine("Geef de kleur van de tweede ring in:");
            string input2 = Console.ReadLine();
            int input2ToInt = 0;
            switch (input2)
            {
                case "zwart":
                    input2ToInt = 0;
                    break;
                case "bruin":
                    input2ToInt = 1;
                    break;
                case "rood":
                    input2ToInt = 2;
                    break;
                case "oranje":
                    input2ToInt = 3;
                    break;
                case "geel":
                    input2ToInt = 4;
                    break;
                case "groen":
                    input2ToInt = 5;
                    break;
                case "blauw":
                    input2ToInt = 6;
                    break;
                case "paars":
                    input2ToInt = 7;
                    break;
                case "grijs":
                    input2ToInt = 8;
                    break;
                case "wit":
                    input2ToInt = 9;
                    break;
            }
            Console.WriteLine("Geef de kleur van de derde ring in:");
            string input3 = Console.ReadLine();
            double input3ToInt = 0;
            switch (input3)
            {
                case "grijs":
                    input3ToInt = 0.01;
                    break;
                case "olijfgroen":
                    input3ToInt = 0.1;
                    break;
                case "zwart":
                    input3ToInt = 1;
                    break;
                case "bruin":
                    input3ToInt = 10;
                    break;
                case "rood":
                    input3ToInt = 100;
                    break;
                case "oranje":
                    input3ToInt = 1000;
                    break;
                case "geel":
                    input3ToInt = 10000;
                    break;
                case "groen":
                    input3ToInt = 100000;
                    break;
                case "blauw":
                    input3ToInt = 1000000;
                    break;
                case "paars":
                    input3ToInt = 10000000;
                    break;
            }

            string hetGetal = Convert.ToString(input1ToInt) + Convert.ToString(input2ToInt);
            double hetGetalToDouble = Convert.ToDouble(hetGetal);

            double weerstand = hetGetalToDouble * input3ToInt;
            Console.WriteLine($"Deze weerstand heeft waarde van {weerstand} Ohm.");
            Console.ReadLine();
        }
    }
}
