﻿using System;

namespace H4_orakeltje_van_Delphi_deel_2
{
    class Program
    {
        static void Main(string[] args)
        {
            Random rnd = new Random();

            Console.WriteLine("Ben je een man (m) of vrouw (v)?");
            char geslacht = Console.ReadKey().KeyChar; // Readkey leest één karakter en met keyChar wordt dat karakter teruggegeven in de console
            Console.WriteLine();
            Console.WriteLine("Hoe oud ben je?");
            byte leeftijd = byte.Parse(Console.ReadLine());
            int maximaleLeeftijdVrouw = 150;
            int maximaleLeeftijdMan = 120;

            switch (geslacht)
            {
                case (char)Sexes.Female:
                    int aantalJaarMogelijk = maximaleLeeftijdVrouw - leeftijd;
                    int aantalJaar = rnd.Next(5, aantalJaarMogelijk);
                    Console.WriteLine($"Je zal nog {aantalJaar} jaar leven.");
                    break;
                case (char)Sexes.Male:
                    aantalJaarMogelijk = maximaleLeeftijdMan - leeftijd;
                    aantalJaar = rnd.Next(5, aantalJaarMogelijk);
                    Console.WriteLine($"Je zal nog {aantalJaar} jaar leven.");
                    break;
                default:
                    Console.WriteLine("We kunnen enkel berekenen als je man of vrouw opgeeft.");
                    break;
            }
            Console.ReadLine();
        }
    }
}