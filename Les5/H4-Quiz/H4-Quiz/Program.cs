﻿using System;

namespace H4_Quiz
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.BackgroundColor = ConsoleColor.DarkRed;
            Console.WriteLine("In welke maand begint de herfst?");
            Console.ResetColor();
            Console.ForegroundColor = ConsoleColor.DarkCyan;
            Console.WriteLine("a) Januari");
            Console.WriteLine("b) Oktober");
            Console.WriteLine("c) September");
            Console.WriteLine("d) December");
            Console.ResetColor();
            string antwoord1 = Console.ReadLine();
            int juist = 0;
            int fout = 0;
            switch (antwoord1)
            {
                case "a":
                    Console.BackgroundColor = ConsoleColor.Red;
                    Console.WriteLine("Het antwoord is fout!");
                    Console.ResetColor();
                    fout = 1;
                    break;
                case "b":
                    Console.BackgroundColor = ConsoleColor.Red;
                    Console.WriteLine("Het antwoord is fout!");
                    Console.ResetColor();
                    fout = 1;
                    break;
                case "c":
                    Console.BackgroundColor = ConsoleColor.Green;
                    Console.WriteLine("Het antwoord is correct!");
                    Console.ResetColor();
                    juist = 2;
                    break;
                case "d":
                    Console.BackgroundColor = ConsoleColor.Red;
                    Console.WriteLine("Het antwoord is fout!");
                    Console.ResetColor();
                    fout = 1;
                    break;
            }

            Console.BackgroundColor = ConsoleColor.DarkRed;
            Console.WriteLine("Hoe heet de vriend van de pop Barbie?");
            Console.ResetColor();
            Console.ForegroundColor = ConsoleColor.DarkCyan;
            Console.WriteLine("a) Jan");
            Console.WriteLine("b) John");
            Console.WriteLine("c) Ken");
            Console.WriteLine("d) Larry");
            Console.ResetColor();
            string antwoord2 = Console.ReadLine();
            switch (antwoord2)
            {
                case "a":
                    Console.BackgroundColor = ConsoleColor.Red;
                    Console.WriteLine("Het antwoord is fout!");
                    Console.ResetColor();
                    fout += 1;
                    break;
                case "b":
                    Console.BackgroundColor = ConsoleColor.Red;
                    Console.WriteLine("Het antwoord is fout!");
                    Console.ResetColor();
                    fout += 1;
                    break;
                case "c":
                    Console.BackgroundColor = ConsoleColor.Green;
                    Console.WriteLine("Het antwoord is correct!");
                    Console.ResetColor();
                    juist += 2;
                    break;
                case "d":
                    Console.BackgroundColor = ConsoleColor.Red;
                    Console.WriteLine("Het antwoord is fout!");
                    Console.ResetColor();
                    fout += 1;
                    break;
            }

            Console.BackgroundColor = ConsoleColor.DarkRed;
            Console.WriteLine("Waar is iemand met claustrofobie bang voor?");
            Console.ResetColor();
            Console.ForegroundColor = ConsoleColor.DarkCyan;
            Console.WriteLine("a) Keuzes maken");
            Console.WriteLine("b) Kluizen");
            Console.WriteLine("c) Muizen");
            Console.WriteLine("d) Kleine ruimtes");
            Console.ResetColor();
            string antwoord3 = Console.ReadLine();
            switch (antwoord3)
            {
                case "a":
                    Console.BackgroundColor = ConsoleColor.Red;
                    Console.WriteLine("Het antwoord is fout!");
                    Console.ResetColor();
                    fout += 1;
                    break;
                case "b":
                    Console.BackgroundColor = ConsoleColor.Red;
                    Console.WriteLine("Het antwoord is fout!");
                    Console.ResetColor();
                    fout += 1;
                    break;
                case "c":
                    Console.BackgroundColor = ConsoleColor.Red;
                    Console.WriteLine("Het antwoord is fout!");
                    Console.ResetColor();
                    fout += 1;
                    break;
                case "d":
                    Console.BackgroundColor = ConsoleColor.Green;
                    Console.WriteLine("Het antwoord is correct!");
                    Console.ResetColor();
                    juist += 2;
                    break;
            }

            int score = juist - fout;
            Console.BackgroundColor = ConsoleColor.DarkCyan;
            Console.WriteLine($"Je score is {score}");
            Console.ResetColor();
            Console.ReadLine();
        }
    }
}
