﻿using System;

namespace H4_BMI_met_if
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hoeveel weeg je in kg?");
            double gewicht = double.Parse(Console.ReadLine());

            Console.WriteLine("Hoe groot ben je in m?");
            double lengte = double.Parse(Console.ReadLine());

            double bmi = gewicht / Math.Pow(lengte, 2);
            Console.WriteLine($"Je BMI bedraagt {Math.Round(bmi, 2)}.");

            if (bmi < 18.5)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("ondergewicht");
            }
            else if (bmi >= 18.5 && bmi <= 24.9)
            {
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("normaal gewicht");
            }
            else if (bmi >= 25 && bmi <= 29.9)
            {
                Console.ForegroundColor = ConsoleColor.DarkYellow;
                Console.WriteLine("overgewicht. Je loopt niet echt een risico, maar je mag niet dikker worden.");
            }
            else if (bmi >= 30 && bmi <= 39.9)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Zwaarlijvigheid (obesitas). Verhoogde kans op allerlei aandoeningen zoals diabetes, hartaandoeningen en rugklachten. Je zou 5 tot 10 kg moeten vermageren.");
            }
            else
            {
                Console.ForegroundColor = ConsoleColor.Magenta;
                Console.WriteLine("ernstige zwaarlijvigheid. Je moet dringend vermageren want je gezondheid is in gevaar (of je hebt je lengte of gewicht in verkeerde eenheid ingevoerd)");
            }
            Console.ReadLine();
        }
    }
}
