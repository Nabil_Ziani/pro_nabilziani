﻿using System;

namespace H4_Ohm_berekenaar
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Wat wens je te berekenen, spanning, weerstand of stroomsterkte?");
            string keuzeGebruiker = Console.ReadLine();

            if (keuzeGebruiker == "spanning")
            {
                Console.WriteLine("Geef de stroomsterkte in:");
                int stroomsterkte = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine("Geef de weerstand in:");
                int weerstand = Convert.ToInt32(Console.ReadLine());

                int spanning = stroomsterkte * weerstand;
                Console.WriteLine($"De spanning = {spanning}");
                Console.ReadLine();
            }
            else if (keuzeGebruiker == "weerstand")
            {
                Console.WriteLine("Geef de spanning in:");
                int spanning = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine("Geef de stroomsterkte in:");
                int stroomsterkte = Convert.ToInt32(Console.ReadLine());

                int weerstand = spanning / stroomsterkte;
                Console.WriteLine($"De weerstand = {weerstand}");
                Console.ReadLine();
            }
            else if (keuzeGebruiker == "stroomsterkte")
            {
                Console.WriteLine("Geef de spanning in:");
                int spanning = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine("Geef de weerstand in:");
                int weerstand = Convert.ToInt32(Console.ReadLine());

                int stroomsterkte = spanning / weerstand;
                Console.WriteLine($"De stroomsterkte = {stroomsterkte}");
                Console.ReadLine();
            }
            else
            {
                Console.WriteLine("Gelieve één van de gegeven opties in te geven!");
                Console.ReadLine();
            }
        }
    }
}
