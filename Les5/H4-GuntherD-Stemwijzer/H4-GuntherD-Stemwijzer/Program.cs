﻿using System;

namespace H4_GuntherD_Stemwijzer
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Werk je veel?");
            string antwoord1 = Console.ReadLine();
            if (antwoord1 == "ja")
            {
                Console.WriteLine("Koop je soms bruin brood?");
                string antwoord2 = Console.ReadLine();
                if (antwoord2 == "ja")
                {
                    Console.WriteLine("Ben je een seut?");
                    string antwoord3 = Console.ReadLine();
                    if (antwoord3 == "ja")
                    {
                        Console.BackgroundColor = ConsoleColor.DarkRed;
                        Console.WriteLine("Je stemt op CD&V");
                        Console.ResetColor();
                    }
                    else
                    {
                        Console.WriteLine("Heb je vrienden?");
                        string antwoord4 = Console.ReadLine();
                        if (antwoord4 == "ja")
                        {
                            Console.WriteLine("Staat jouw wagen, huis,... op naam van jouw ouders?");
                            string antwoord5 = Console.ReadLine();
                            if (antwoord5 == "ja")
                            {
                                Console.BackgroundColor = ConsoleColor.DarkRed;
                                Console.WriteLine("Je stemt op Open VLD");
                                Console.ResetColor();
                            }
                            else
                            {
                                Console.BackgroundColor = ConsoleColor.DarkRed;
                                Console.WriteLine("Je stemt Blanco");
                                Console.ResetColor();
                            }
                        }
                        else
                        {
                            Console.BackgroundColor = ConsoleColor.DarkRed;
                            Console.WriteLine("Je stemt op NVA");
                            Console.ResetColor();
                        }
                    }
                }
                else
                {
                    Console.BackgroundColor = ConsoleColor.DarkRed;
                    Console.WriteLine("Je stemt op Vlaams Belang");
                    Console.ResetColor();
                }
            }
            else
            {
                Console.WriteLine("Eet je vaak Quinoa");
                string antwoord2 = Console.ReadLine();
                if (antwoord2 == "ja")
                {
                    Console.BackgroundColor = ConsoleColor.DarkRed;
                    Console.WriteLine("Je stemt op Groen");
                    Console.ResetColor();
                }
                else
                {
                    Console.WriteLine("Krijg je vaak de schuld van alles?");
                    string antwoord3 = Console.ReadLine();
                    if (antwoord3 == "ja")
                    {
                        Console.BackgroundColor = ConsoleColor.DarkRed;
                        Console.WriteLine("Je stemt op sp.a");
                        Console.ResetColor();
                    }
                    else
                    {
                        Console.WriteLine("Geloof je nog in Sinterklaas?");
                        string antwoord4 = Console.ReadLine();
                        if (antwoord4 == "ja")
                        {
                            Console.BackgroundColor = ConsoleColor.DarkRed;
                            Console.WriteLine("Je stemt op PVDA");
                            Console.ResetColor();
                        }
                        else
                        {
                            Console.BackgroundColor = ConsoleColor.DarkRed;
                            Console.WriteLine("Je stemt Blanco");
                            Console.ResetColor();
                        }
                    }
                }
            }
            Console.ReadLine();
        }
    }
}
