﻿using System;

namespace H4_Schrikkeljaar
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Voer een jaartal in: ");
            int jaartal = Convert.ToInt32(Console.ReadLine());

            if (jaartal % 4 ==  0 && jaartal % 100 != 0)
            {
                Console.WriteLine("wél schrikkeljaar");
            }
            else if (jaartal % 400 == 0)
            {
                Console.WriteLine("wél schrikkeljaar");
            }
            else
            {
                Console.WriteLine("geen schrikkeljaar");
            }
            Console.ReadLine();
        }
    }
}
