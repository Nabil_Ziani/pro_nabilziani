﻿using System;

namespace H4_Schoenverkoper
{
    class Program
    {
        static void Main(string[] args)
        {
            int prijsSchoen = 20;
            int prijsSchoenKorting = 10;

            Console.WriteLine("Vanaf hoeveel paar krijgt de klant korting?");
            int bepalingKorting = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("Hoeveel paar schoenen wens je te kopen?");
            int aantalSchoenen = Convert.ToInt32(Console.ReadLine());

            if (aantalSchoenen < bepalingKorting)
            {
                Console.WriteLine($"De totale prijs is {aantalSchoenen * prijsSchoen} euro.");
            }
            else
            {
                Console.WriteLine($"De totale prijs is {aantalSchoenen * prijsSchoenKorting} euro.");
            }
            Console.ReadLine();
        }
    }
}
