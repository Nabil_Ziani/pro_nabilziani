﻿using System;

namespace Dobbelspel
{
    class Program
    {
        static string Dobbelsteen1()
        {
            string dobbelsteen1 = $@"╔════════╗
║        ║
║    *   ║
║        ║
╚════════╝";
            return dobbelsteen1;
        }
        static string Dobbelsteen2()
        {
            string dobbelsteen2 = $@"╔════════╗
║      * ║
║        ║
║*       ║
╚════════╝";
            return dobbelsteen2;
        }
        static string Dobbelsteen3()
        {
            string dobbelsteen3 = $@"╔════════╗
║      * ║
║   *    ║
║*       ║
╚════════╝";
            return dobbelsteen3;
        }
        static string Dobbelsteen4()
        {
            string dobbelsteen4 = $@"╔════════╗
║*      *║
║        ║
║*      *║
╚════════╝";
            return dobbelsteen4;
        }
        static string Dobbelsteen5()
        {
            string dobbelsteen5 = $@"╔════════╗
║*      *║
║*      *║
║   *    ║
╚════════╝";
            return dobbelsteen5;
        }
        static string Dobbelsteen6()
        {
            string dobbelsteen6 = $@"╔════════╗
║*     * ║
║*     * ║
║*     * ║
╚════════╝";
            return dobbelsteen6;
        }
        static void Main(string[] args)
        {
            Console.WriteLine(Dobbelsteen1());
            Console.WriteLine(Dobbelsteen2());
            Console.WriteLine(Dobbelsteen3());
            Console.WriteLine(Dobbelsteen4());
            Console.WriteLine(Dobbelsteen5());
            Console.WriteLine(Dobbelsteen6());

            Random rnd = new Random();
            string[] keuzeLijst = {Dobbelsteen1(), Dobbelsteen2(), Dobbelsteen3(), Dobbelsteen4(), Dobbelsteen5(), Dobbelsteen6() };

            Console.WriteLine("Nu printen we 2 random dobbelstenen: ");
            Console.WriteLine(keuzeLijst[rnd.Next(0, 6)]);
            Console.WriteLine(keuzeLijst[rnd.Next(0, 6)]);
            Console.ReadLine();
        }
    }
}
