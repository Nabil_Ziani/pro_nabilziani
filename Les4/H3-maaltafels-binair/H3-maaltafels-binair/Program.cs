﻿using System;

namespace H3_maaltafels_binair
{
    class Program
    {
        static void Main(string[] args)
        {
            for (int i = 1; i <= 10; i++)
            {
                string iBinair = Convert.ToString(i, 2);
                int product = RandomGenerator() * i;

                Console.WriteLine($"{iBinair} * {Convert.ToString(RandomGenerator(), 2)} is {Convert.ToString(product, 2)}");
                Console.ReadLine();
            }
        }
        static int RandomGenerator()
        {
            Random rnd = new Random();
            int getal = rnd.Next(1, 11);
            return getal;
        }
    }
}
