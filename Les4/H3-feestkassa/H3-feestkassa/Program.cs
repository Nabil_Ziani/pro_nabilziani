﻿using System;

namespace H3_feestkassa
{
    class Program
    {
        static void Main(string[] args)
        {
            const int PrijsFrietjes = 20;
            const int PrijsKoninginnenhapjes = 10;
            const int PrijsIjsje = 3;
            const int PrijsDrank = 2;

            Console.WriteLine("Frietjes?");
            int aantalFrietjes = Convert.ToInt32(Console.ReadLine());
            int huidigBedrag = (PrijsFrietjes * aantalFrietjes);

            Console.WriteLine("Koninginnenhapjes?");
            int aantalKoninginnenhapjes = Convert.ToInt32(Console.ReadLine());
            huidigBedrag += (PrijsKoninginnenhapjes * aantalKoninginnenhapjes);

            Console.WriteLine("Ijsjes?");
            int aantalIjsjes = Convert.ToInt32(Console.ReadLine());
            huidigBedrag += (PrijsIjsje * aantalIjsjes);

            Console.WriteLine("Dranken?");
            int aantalDranken = Convert.ToInt32(Console.ReadLine());
            huidigBedrag += (PrijsDrank * aantalDranken);

            Console.WriteLine($"Het totaal te betalen bedrag is {huidigBedrag} EURO");
            Console.ReadLine();
        }
    }
}
