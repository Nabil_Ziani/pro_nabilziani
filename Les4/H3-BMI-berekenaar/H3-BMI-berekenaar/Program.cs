﻿using System;

namespace H3_BMI_berekenaar
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hoeveel weeg je in kg?");
            double gewicht = double.Parse(Console.ReadLine());

            Console.WriteLine("Hoe groot ben je in m?");
            double lengte = double.Parse(Console.ReadLine());

            double bmi = gewicht / Math.Pow(lengte, 2);
            Console.WriteLine($"Je BMI bedraagt {Math.Round(bmi, 2)}.");
            Console.ReadLine();
        }
    }
}
