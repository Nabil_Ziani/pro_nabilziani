﻿using System;

namespace H3_op_de_poef
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Voer bedrag in: ");
            int bedrag1 = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine($"De poef staat op {bedrag1} euro");

            int huidigBedrag = bedrag1;

            Console.WriteLine("Voer bedrag in: ");
            int bedrag2 = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine($"De poef staat op {huidigBedrag + bedrag2} euro");

            huidigBedrag += bedrag2;

            Console.WriteLine("Voer bedrag in: ");
            int bedrag3 = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine($"De poef staat op {huidigBedrag + bedrag3} euro");

            huidigBedrag += bedrag3;

            Console.WriteLine("Voer bedrag in: ");
            int bedrag4 = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine($"De poef staat op {huidigBedrag + bedrag4} euro");

            huidigBedrag += bedrag4;

            Console.WriteLine("Voer bedrag in: ");
            int bedrag5 = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine($"De poef staat op {huidigBedrag + bedrag5} euro");

            huidigBedrag += bedrag5;

            Console.WriteLine("************************************");
            Console.WriteLine($"Het totaal van de poef is {huidigBedrag} euro");

            double afbetalingTijd = (double)huidigBedrag / 10;
            Console.WriteLine($"Dit zal {Math.Round(afbetalingTijd)} afbetalingen vragen.");
            Console.ReadLine();
        }
    }
}
