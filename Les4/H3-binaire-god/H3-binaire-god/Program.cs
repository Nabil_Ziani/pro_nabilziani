﻿using System;

namespace H3_binaire_god
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Geef een getal:");
            int getal = Convert.ToInt32(Console.ReadLine());

            getal = getal - (getal % 2);
            Console.WriteLine($"Het getal is nu {getal}.");

            getal = getal * 8;
            Console.WriteLine($"Het getal is nu {getal}.");

            Console.WriteLine("Geef een binaire string:");
            string inputBinair = Console.ReadLine();

            int inputBinairOmgezet = Convert.ToInt32(inputBinair, 2);
            inputBinairOmgezet = inputBinairOmgezet - (inputBinairOmgezet % 2);

            inputBinair = Convert.ToString(inputBinairOmgezet, 2);
            Console.WriteLine($"Het getal is nu {inputBinair}.");

            inputBinairOmgezet = inputBinairOmgezet * 8;
            inputBinair = Convert.ToString(inputBinairOmgezet, 2);
            Console.WriteLine($"Het getal is nu {inputBinair}.");
            Console.ReadLine();
        }
    }
}
