﻿using System;

namespace H3_random_invoer
{
    class Program
    {
        static void Main(string[] args)
        {
            Random rnd = new Random();
            Console.WriteLine("Voer bedrag in: ");
            int bedrag1 = rnd.Next(1, 51);
            Console.WriteLine($"Automatisch gegenereerd: {bedrag1}");
            Console.WriteLine($"De poef staat op {bedrag1} euro");

            int huidigBedrag = bedrag1;

            Console.WriteLine("Voer bedrag in: ");
            int bedrag2 = rnd.Next(1, 51);
            Console.WriteLine($"Automatisch gegenereerd: {bedrag2}");
            Console.WriteLine($"De poef staat op {huidigBedrag + bedrag2} euro");

            huidigBedrag += bedrag2;

            Console.WriteLine("Voer bedrag in: ");
            int bedrag3 = rnd.Next(1, 51);
            Console.WriteLine($"Automatisch gegenereerd: {bedrag3}");
            Console.WriteLine($"De poef staat op {huidigBedrag + bedrag3} euro");

            huidigBedrag += bedrag3;

            Console.WriteLine("Voer bedrag in: ");
            int bedrag4 = rnd.Next(1, 51);
            Console.WriteLine($"Automatisch gegenereerd: {bedrag4}");
            Console.WriteLine($"De poef staat op {huidigBedrag + bedrag4} euro");

            huidigBedrag += bedrag4;

            Console.WriteLine("Voer bedrag in: ");
            int bedrag5 = rnd.Next(1, 51);
            Console.WriteLine($"Automatisch gegenereerd: {bedrag5}");
            Console.WriteLine($"De poef staat op {huidigBedrag + bedrag5} euro");

            huidigBedrag += bedrag5;

            Console.WriteLine("************************************");
            Console.WriteLine($"Het totaal van de poef is {huidigBedrag} euro");

            double afbetalingTijd = (double)huidigBedrag / 10;
            Console.WriteLine($"Dit zal {Math.Round(afbetalingTijd)} afbetalingen vragen.");
            Console.ReadLine();
        }
    }
}
