﻿using System;

namespace H3_orakeltje
{
    class Program
    {
        static void Main(string[] args)
        {
            Random rnd = new Random();
            int aantalJaar = rnd.Next(5, 125);

            Console.WriteLine($"Je zal nog {aantalJaar} jaar leven.");
            Console.ReadLine();
        }
    }
}
