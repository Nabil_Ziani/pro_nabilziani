﻿using System;

namespace H3_geometric_fun
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Geef een hoek, uitgedrukt in graden:");
            int inputGraden = Convert.ToInt32(Console.ReadLine());

            double inputRadialen = inputGraden * (Math.PI / 180);
            Console.WriteLine($"{inputGraden} graden is {Math.Round(inputRadialen, 2)} radialen.");

            double sinus = Math.Sin(inputRadialen);
            double cosinus = Math.Cos(inputRadialen);
            double tangens = Math.Tan(inputRadialen);
            Console.WriteLine($"De sinus is {Math.Round(sinus, 2)}.");
            Console.WriteLine($"De cosinus is {Math.Round(cosinus, 2)}.");
            Console.WriteLine($"De tangens is {Math.Round(tangens, 2)}.");
            Console.ReadLine();
        }
    }
}
