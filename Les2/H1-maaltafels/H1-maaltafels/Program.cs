﻿using System;

namespace H1_maaltafels
{
    class Program
    {
        static void Main(string[] args)
        {
            int getal = 411;

            for (int i = 1; i <= 10; i++)
            {
                Console.Clear();
                Console.WriteLine($"{i} * {getal} is {i * getal}");
                Console.ReadLine();
            }
        }
    }
}
