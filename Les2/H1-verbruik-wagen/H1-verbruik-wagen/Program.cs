﻿using System;

namespace H1_verbruik_wagen
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Geef het aantal liter in tank voor de rit: ");
            double aantalLiterVoor = Convert.ToDouble(Console.ReadLine());

            Console.Write("Geef het aantal liter in tank na de rit: ");
            double aantalLiterNa = Convert.ToDouble(Console.ReadLine());

            Console.Write("Geef kilometerstand1 van je auto voor de rit: ");
            double kilometerstandVoor = Convert.ToDouble(Console.ReadLine());

            Console.Write("Geef kilometerstand2 van je auto na de rit: ");
            double kilomerstandNa = Convert.ToDouble(Console.ReadLine());

            double gemiddeldVerbruik = (100 * (aantalLiterVoor - aantalLiterNa)/(kilomerstandNa - kilometerstandVoor));

            Console.WriteLine("");
            Console.WriteLine($"Het verbuik van de auto is: {gemiddeldVerbruik}");
            Console.WriteLine($"Het afgeronde verbruik van de auto is {Math.Round(gemiddeldVerbruik, 2)}");
            Console.ReadLine();
        }
    }
}
