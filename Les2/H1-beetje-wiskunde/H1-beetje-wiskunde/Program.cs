﻿using System;

namespace H1_beetje_wiskunde
{
    class Program
    {
        static void Main(string[] args)
        {
            float resultaat1 = -1 + 4 * 6;
            float resultaat2 = (35 + 5) % 7;
            float resultaat3 = 14 + -4 * 6 / 11;
            float resultaat4 = 2 + 15 / 6 * 1 - 7 % 2;

            Console.WriteLine(resultaat1);
            Console.WriteLine(resultaat2);
            Console.WriteLine(resultaat3);
            Console.WriteLine(resultaat4);
            Console.ReadLine();
        }
    }
}
