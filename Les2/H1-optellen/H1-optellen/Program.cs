﻿using System;

namespace H1_optellen
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Geef het eerste getal:");
            int getal1 = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("Geef het tweede getal:");
            int getal2 = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine($"De som is: {getal1 + getal2}");
            Console.ReadLine();
        }
    }
}
