﻿using System;

namespace H1_variabelen_hoofdletters
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Wat wilt u herhaald zien worden in hoofdletters?");
            string userInput = Console.ReadLine();

            Console.WriteLine($"Uw invoer in hoofdletters: {userInput.ToUpper()}");
            Console.WriteLine("Druk op een willekeurige toets om het programma te beëindigen...");
            Console.ReadLine();
        }
    }
}
