﻿using System;
using System.Drawing;

namespace H2_weerstandsberekenaar_deel3
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Geef de waarde (uitgedrukt in een getal van 0 tot 9) van de eerste ring: ");
            string ring1 = Console.ReadLine();
            Console.Write("Geef de waarde (uitgedrukt in een getal van 0 tot 9) van de tweede ring: ");
            string ring2 = Console.ReadLine();
            Console.Write("Geef de waarde (uitgedrukt in een getal van -2 tot 7) van de derde ring (exponent): ");
            int ring3 = Convert.ToInt32(Console.ReadLine());

            int hetGetal = Convert.ToInt32(ring1 + ring2);
            double vermenigvuldigingsFactor = Math.Pow(10, ring3);
            double totaal = (hetGetal * vermenigvuldigingsFactor);

            Console.WriteLine("╔═════════╦═════════╦═════════╦══════════════╗");
            Console.WriteLine("║  ring1  ║  ring2  ║  ring3  ║ Total (Ohm)  ║");
            Console.WriteLine("╟─────────╫─────────╫─────────╫──────────────╫");
            Console.Write("║ ");
            switch (Convert.ToInt32(ring1))
            {
                case 0:
                    Console.ForegroundColor = ConsoleColor.Black;
                    Console.Write($"{ ring1}       ");
                    Console.ResetColor();
                    break;
                case 1:
                    Console.ForegroundColor = ConsoleColor.Cyan;
                    Console.Write($"{ ring1}       ");
                    Console.ResetColor();
                    break;
                case 2:
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.Write($"{ ring1}       ");
                    Console.ResetColor();
                    break;
                case 3:
                    Console.ForegroundColor = ConsoleColor.DarkYellow;
                    Console.Write($"{ ring1}       ");
                    Console.ResetColor();
                    break;
                case 4:
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    Console.Write($"{ ring1}       ");
                    Console.ResetColor();
                    break;
                case 5:
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.Write($"{ ring1}       ");
                    Console.ResetColor();
                    break;
                case 6:
                    Console.ForegroundColor = ConsoleColor.Blue;
                    Console.Write($"{ ring1}       ");
                    Console.ResetColor();
                    break;
                case 7:
                    Console.ForegroundColor = ConsoleColor.Magenta;
                    Console.Write($"{ ring1}       ");
                    Console.ResetColor();
                    break;
                case 8:
                    Console.ForegroundColor = ConsoleColor.Gray;
                    Console.Write($"{ ring1}       ");
                    Console.ResetColor();
                    break;
                case 9:
                    Console.ForegroundColor = ConsoleColor.White;
                    Console.Write($"{ ring1}       ");
                    Console.ResetColor();
                    break;
            }
            Console.Write("║ ");
            switch (Convert.ToInt32(ring2))
            {
                case 0:
                    Console.ForegroundColor = ConsoleColor.Black;
                    Console.Write($"{ ring2}       ");
                    Console.ResetColor();
                    break;
                case 1:
                    Console.ForegroundColor = ConsoleColor.Cyan;
                    Console.Write($"{ ring2}       ");
                    Console.ResetColor();
                    break;
                case 2:
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.Write($"{ ring2}       ");
                    Console.ResetColor();
                    break;
                case 3:
                    Console.ForegroundColor = ConsoleColor.DarkYellow;
                    Console.Write($"{ ring2}       ");
                    Console.ResetColor();
                    break;
                case 4:
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    Console.Write($"{ ring2}       ");
                    Console.ResetColor();
                    break;
                case 5:
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.Write($"{ ring2}       ");
                    Console.ResetColor();
                    break;
                case 6:
                    Console.ForegroundColor = ConsoleColor.Blue;
                    Console.Write($"{ ring2}       ");
                    Console.ResetColor();
                    break;
                case 7:
                    Console.ForegroundColor = ConsoleColor.Magenta;
                    Console.Write($"{ ring2}       ");
                    Console.ResetColor();
                    break;
                case 8:
                    Console.ForegroundColor = ConsoleColor.Gray;
                    Console.Write($"{ ring2}       ");
                    Console.ResetColor();
                    break;
                case 9:
                    Console.ForegroundColor = ConsoleColor.White;
                    Console.Write($"{ ring2}       ");
                    Console.ResetColor();
                    break;
            }
            Console.Write("║ ");
            switch (ring3)
            {
                case -2:
                    Console.ForegroundColor = ConsoleColor.Gray;
                    Console.Write($"{ ring3}       ");
                    Console.ResetColor();
                    break;
                case -1:
                    Console.ForegroundColor = ConsoleColor.DarkYellow;
                    Console.Write($"{ ring3}       ");
                    Console.ResetColor();
                    break;
                case 0:
                    Console.ForegroundColor = ConsoleColor.Black;
                    Console.Write($"{ ring3}       ");
                    Console.ResetColor();
                    break;
                case 1:
                    Console.ForegroundColor = ConsoleColor.DarkCyan;
                    Console.Write($"{ ring3}       ");
                    Console.ResetColor();
                    break;
                case 2:
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.Write($"{ ring3}       ");
                    Console.ResetColor();
                    break;
                case 3:
                    Console.ForegroundColor = ConsoleColor.DarkRed;
                    Console.Write($"{ ring3}       ");
                    Console.ResetColor();
                    break;
                case 4:
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    Console.Write($"{ ring3}       ");
                    Console.ResetColor();
                    break;
                case 5:
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.Write($"{ ring3}       ");
                    Console.ResetColor();
                    break;
                case 6:
                    Console.ForegroundColor = ConsoleColor.Blue;
                    Console.Write($"{ ring3}       ");
                    Console.ResetColor();
                    break;
                case 7:
                    Console.ForegroundColor = ConsoleColor.Magenta;
                    Console.Write($"{ ring3}       ");
                    Console.ResetColor();
                    break;
            }
            Console.Write("║   ");
            Console.Write($"{totaal}        ");
            Console.WriteLine("║");
            Console.WriteLine("╚═════════╩═════════╩═════════╩══════════════╝");

            Console.ReadLine();
        }
    }
}
