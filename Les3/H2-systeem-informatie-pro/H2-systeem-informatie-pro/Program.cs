﻿using System;
using System.IO;

namespace H2_systeem_informatie_pro
{
    class Program
    {
        static void Main(string[] args)
        {
            long cFreeSpace = DriveInfo.GetDrives()[0].AvailableFreeSpace;
            long cTotalSpace = DriveInfo.GetDrives()[0].TotalSize;

            Console.WriteLine($"Vrije ruimte op jouw c-schijf: {cFreeSpace}");
            Console.WriteLine($"Totale ruimte van jouw c-schijf: {cTotalSpace}");
            Console.WriteLine();
            Console.WriteLine("****************************************************************");

            Console.Write($"Geef met nummer 1 t/m 2 aan welke harde schijf van jouw pc je info wenst: ");
            int userInput = Convert.ToInt32(Console.ReadLine()) -1;
            long freeSpace = DriveInfo.GetDrives()[userInput].AvailableFreeSpace;
            Console.WriteLine($"De vrije ruimte van C:\\ is {freeSpace / 1000000000} Gb.");
            Console.ReadLine();
        }
    }
}
