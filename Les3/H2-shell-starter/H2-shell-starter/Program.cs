﻿using System;

namespace H2_shell_starter
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Welke van volgende shell-commando's wil je uitvoeren: ");
            Console.WriteLine("ipconfig /all | arp -a | hostname | getmac | nslookup google.com");
            string userChoice = Console.ReadLine();

            if (userChoice == "ipconfig /all")
            {
                IpConfig();
            }
            else if (userChoice == "arp -a")
            {
                Arp();
            }
            else if (userChoice == "hostname")
            {
                HostName();
            }
            else if (userChoice == "getmac")
            {
                GetMac();
            }
            else if (userChoice == "nslookup google.com")
            {
                NsLookup();
            }
            else
            {
                Console.WriteLine("Gelieve één van de opgegeven commando's in te geven...");
            }
        }
        static void IpConfig()
        {
            System.Diagnostics.Process process = new System.Diagnostics.Process();
            process.StartInfo.FileName = "ipconfig";
            process.StartInfo.Arguments = "/all";
            process.StartInfo.UseShellExecute = false;
            process.StartInfo.RedirectStandardOutput = true;
            process.StartInfo.RedirectStandardError = true;
            process.Start();

            string output = process.StandardOutput.ReadToEnd();
        Console.WriteLine(output);

            string error = process.StandardError.ReadToEnd();
        Console.WriteLine(error); 
            Console.ReadLine(); 
        }
        static void Arp()
        {
            System.Diagnostics.Process process2 = new System.Diagnostics.Process();
            process2.StartInfo.FileName = "arp";
            process2.StartInfo.Arguments = "-a";
            process2.StartInfo.UseShellExecute = false;
            process2.StartInfo.RedirectStandardOutput = true;
            process2.StartInfo.RedirectStandardError = true;
            process2.Start();

            string output2 = process2.StandardOutput.ReadToEnd();
            Console.WriteLine(output2);

            string error2 = process2.StandardError.ReadToEnd();
            Console.WriteLine(error2);
            Console.ReadLine();
        }
        static void GetMac()
        {
            System.Diagnostics.Process process3 = new System.Diagnostics.Process();
            process3.StartInfo.FileName = "getmac";
            process3.StartInfo.UseShellExecute = false;
            process3.StartInfo.RedirectStandardOutput = true;
            process3.StartInfo.RedirectStandardError = true;
            process3.Start();

            string output3 = process3.StandardOutput.ReadToEnd();
            Console.WriteLine(output3);

            string error3 = process3.StandardError.ReadToEnd();
            Console.WriteLine(error3);
            Console.ReadLine();
        }
        static void HostName()
        {
            System.Diagnostics.Process process1 = new System.Diagnostics.Process();
            process1.StartInfo.FileName = "hostname";
            process1.StartInfo.UseShellExecute = false;
            process1.StartInfo.RedirectStandardOutput = true;
            process1.StartInfo.RedirectStandardError = true;
            process1.Start();

            string output1 = process1.StandardOutput.ReadToEnd();
            Console.WriteLine(output1);

            string error1 = process1.StandardError.ReadToEnd();
            Console.WriteLine(error1);
            Console.ReadLine();
        }
        static void NsLookup()
        {
            System.Diagnostics.Process process4 = new System.Diagnostics.Process();
            process4.StartInfo.FileName = "nslookup";
            process4.StartInfo.Arguments = "google.com";
            process4.StartInfo.UseShellExecute = false;
            process4.StartInfo.RedirectStandardOutput = true;
            process4.StartInfo.RedirectStandardError = true;
            process4.Start();

            string output4 = process4.StandardOutput.ReadToEnd();
            Console.WriteLine(output4);

            string error4 = process4.StandardError.ReadToEnd();
            Console.WriteLine(error4);
            Console.ReadLine();
        }
    }
}
