﻿using System;

namespace H2_weerstandberekenaar_deel2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.OutputEncoding = System.Text.Encoding.UTF8;
            Console.Write("Geef de waarde (uitgedrukt in een getal van 0 tot 9) van de eerste ring: ");
            string ring1 = Console.ReadLine();
            Console.Write("Geef de waarde (uitgedrukt in een getal van 0 tot 9) van de tweede ring: ");
            string ring2 = Console.ReadLine();
            Console.Write("Geef de waarde (uitgedrukt in een getal van -2 tot 7) van de derde ring (exponent): ");
            int ring3 = Convert.ToInt32(Console.ReadLine());

            int hetGetal = Convert.ToInt32(ring1 + ring2);
            double vermenigvuldigingsFactor = Math.Pow(10, ring3);
            double total = (hetGetal * vermenigvuldigingsFactor);

            string table = $@"
             ╔═════════╦═════════╦═════════╦══════════════╗
             ║  ring1  ║  ring2  ║  ring3  ║ Total (Ohm)  ║
             ╟─────────╫─────────╫─────────╫──────────────╫
             ║ {ring1}       ║ {ring2}       ║ {ring3}       ║   {total}        ║
             ╚═════════╩═════════╩═════════╩══════════════╝";

            Console.WriteLine(table);
            Console.ReadLine();
        }
    }
}
