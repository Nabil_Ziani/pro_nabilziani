﻿using System;

namespace H2_string_interpolation
{
    class Program
    {
        static void Main(string[] args)
        {
            // H1-maaltafels 
            int getal = 411;

            for (int i = 1; i <= 10; i++)
            {
                Console.Clear();
                Console.WriteLine($"{i} * {getal} is {i * getal}");
                Console.ReadLine();
            }
            // H1-ruimte
            Console.WriteLine("Geef je gewicht in: ");
            double userGewicht = Convert.ToDouble(Console.ReadLine());

            Console.WriteLine($"Op Mercurius heb je een schijnbaar gewicht van {userGewicht * 0.38} kg.");
            Console.WriteLine($"Op Venus heb je een schijnbaar gewicht van {userGewicht * 0.91} kg.");
            Console.WriteLine($"Op Aarde heb je een schijnbaar gewicht van {userGewicht * 1.00} kg.");
            Console.WriteLine($"Op Mars heb je een schijnbaar gewicht van {userGewicht * 0.38} kg.");
            Console.WriteLine($"Op Jupiter heb je een schijnbaar gewicht van {userGewicht * 2.34} kg.");
            Console.WriteLine($"Op Saturnus heb je een schijnbaar gewicht van {userGewicht * 1.06} kg.");
            Console.WriteLine($"Op Uranus heb je een schijnbaar gewicht van {userGewicht * 0.92} kg.");
            Console.WriteLine($"Op Neptunus heb je een schijnbaar gewicht van {userGewicht * 1.19} kg.");
            Console.WriteLine($"Op Pluto heb je een schijnbaar gewicht van {userGewicht * 0.06} kg.");
            Console.ReadLine();
        }
    }
}
