﻿using System;

namespace H6_Paswoord_generator_methode
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine($"Het gegenereerde paswoord is: {PaswoordGenerator(10)}");
            Console.ReadLine();
        }

        static string PaswoordGenerator(int lengte)
        {
            Random rnd = new Random();
            string karakters = "abcde0fghijkA9BCDEFGl8mnopq7rsST6UVWXY5Ztuvw4xyzHI3JKLMN2OPQR1";
            string paswoord = "";
            for (int i = 0; i < lengte; i++)
            {
                char paswoordKarakter = karakters[rnd.Next(karakters.Length)]; // kiest een random-karakter uit de string "karakters"
                paswoord += paswoordKarakter;
            }
            return paswoord;
        }
    }
}
