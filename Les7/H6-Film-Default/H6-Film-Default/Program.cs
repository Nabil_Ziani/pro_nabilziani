﻿using System;

namespace H6_Film_Default
{
    class Program
    {
        static void Main(string[] args)
        {
            // Toon aan dat de methode werkt met zowel 1, 2, als 3 parameters.
            FilmRuntime("The Matrix", 120, Genre.Actie);
            FilmRuntime("Fast and Furious", 79);
            FilmRuntime("Angry Birds");

            //Toon aan dat je met 'named arguments' de methode kan aanroepen.
            FilmRuntime(filmGenre: Genre.Avontuur, filmNaam: "Spiderman", filmtijd: 150);

            Console.ReadLine();
        }

        static void FilmRuntime(string filmNaam, int filmtijd = 90, Genre filmGenre = Genre.Onbekend)
        {
            Console.WriteLine($"{filmNaam} ({filmtijd}minuten, {filmGenre})");
        }
    }
}
