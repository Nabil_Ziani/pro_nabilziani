﻿using System;

namespace H6_Grootste_methode
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine(GrootsteMethode(44, 88, 99));
            Console.ReadLine();
        }

        static int GrootsteMethode(int getal1, int getal2, int getal3)
        {
            int grootsteGetal = Math.Max(getal1, getal2);
            grootsteGetal = Math.Max(grootsteGetal, getal3);
            return grootsteGetal;
        }
    }
}
