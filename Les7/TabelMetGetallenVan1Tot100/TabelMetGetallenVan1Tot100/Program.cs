﻿using System;

namespace TabelMetGetallenVan1Tot100
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Tabel met getallen van 1 tot 100");
            for (int i = 1; i <= 100; i++)
            {
                if (i % 10 == 0)
                {
                    Console.Write($"{i}\n");
                } else
                {
                    Console.Write($"{i}\t");
                }
            }
            Console.ReadLine();
        }
    }
}
