﻿using System;

namespace ControleVanDeInvoer
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Controle van de invoer");
            Console.WriteLine("-".PadLeft(50, '-'));

            int aantalOngeldig = 0;
            int aantalGeldig = 0;
            string invoer = "";
            double totaalSomGeldig = 0;

            while (invoer != "n") 
            {
                Console.Write("Voer een getal in tussen 1.0 tot en met 10.0: ");
                double getal = Convert.ToDouble(Console.ReadLine());
                if (getal >= 1.0 && getal <= 10.0)
                {
                    aantalGeldig += 1;
                    totaalSomGeldig += getal;
                }
                else
                {
                    aantalOngeldig += 1;
                }
                Console.Write("Meer getallen invoeren? (j of n): ");
                invoer = Console.ReadLine();
            }
            // Wanneer de gebruiker is gestopt gaan we het resultaat tonen:
            Console.WriteLine($"Er zijn {aantalGeldig} geldig(e) en {aantalOngeldig} ongeldig(e) getallen ingevoerd.");
            Console.WriteLine($"De totale som is {totaalSomGeldig}.");
            Console.ReadLine();
        }
    }
}
