﻿using System;

namespace Methode_IsArmstrong
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Geef een getal in: ");
            string ingegevenGetal = Console.ReadLine();
            GetalIsArmstrong(ingegevenGetal);
        }
        /// <summary>
        /// Methode die bepaald of een getal een Armstrong-getal is
        /// </summary> 
        static void GetalIsArmstrong(string getal)
        {
            double getalApart;
            double berekeningArmstrong = 0;

            double lengteVast = getal.Length;   // Deze variabele wordt doorheen de programma niet veranderd

            int lengte = getal.Length;
            double getalToInt = Convert.ToInt32(getal);

            for (int i = 0; i < lengte; lengte--)
            {
                getalApart = getalToInt / Math.Pow(10, lengte - 1); // Om het eerste getal te krijgen van links naar rechts
                getalApart = Math.Floor(getalApart);
                getalToInt = getalToInt - (getalApart * Math.Pow(10, lengte - 1)); // Om het volgende getal te krijgen

                berekeningArmstrong += Math.Pow(getalApart, lengteVast);
            }
            // Als het getal de som is van zijn eigen cijfers, elk tot de macht verheven van het aantal cijfers dan komen we hierin: 
            if (berekeningArmstrong == int.Parse(getal))
            {
                Console.WriteLine("Het ingegeven getal is een Armstrong-nummer.");
            }
            else
            {
                Console.WriteLine("Het ingegeven getal is géén Armstrong-nummer.");
            }
            Console.ReadLine();
        }
    }
}
