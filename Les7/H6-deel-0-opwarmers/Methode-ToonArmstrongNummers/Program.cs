﻿using System;

namespace Methode_ToonArmstrongNummers
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Geef een getal in: ");
            int maximumGetal = int.Parse(Console.ReadLine());
            ToonArmstrongNummers(maximumGetal);
            Console.ReadLine();
        }
        /// <summary>
        /// Methode die alle Armstrong-nummers toont tot een bepaalde getal
        /// </summary> 
        static double ToonArmstrongNummers(int maxGetal)
        {
            double berekeningArmstrong = 0;
            int getal = 1;
            // De loop gaat elke getal van 1 tot n testen of het een Armstrong-nummer is, zoja wordt het geprint en anders niet 
            while (getal <= maxGetal)  
            {
                double getalApart;
                double lengteVast = Convert.ToString(getal).Length;   // Ik zet getal om naar string om de lengte ervan te kunnen nemen
                int lengte = Convert.ToString(getal).Length;
                double getalToInt = Convert.ToInt32(getal);

                for (int i = 0; i < lengte; lengte--)
                {
                    getalApart = getalToInt / Math.Pow(10, lengte - 1); // Om het eerste getal te krijgen van links naar rechts
                    getalApart = Math.Floor(getalApart);
                    getalToInt = getalToInt - (getalApart * Math.Pow(10, lengte - 1)); // Om het volgende getal te krijgen

                    berekeningArmstrong += Math.Pow(getalApart, lengteVast); // We verheffen het getal (cijfer per cijfer) tot de macht van het aantal cijfers en dit tellen we op; 
                                                                             // als de som hiervan gelijk is aan het getal zelf --> Armstrong-nummer
                }
                // Als het getal de som is van zijn eigen cijfers, elk tot de macht verheven van het aantal cijfers dan komen we hierin: 
                if (berekeningArmstrong == getal)
                {
                    Console.Write($"{berekeningArmstrong} ");
                }
                berekeningArmstrong = 0; // deze variabele na elke loop terug "resetten"
                getal += 1; // getal met elke loop verhogen
            }
            return berekeningArmstrong;
        }
    }
}
