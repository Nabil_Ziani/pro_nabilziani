﻿using System;

namespace Methode_Kwadraat
{
    class Program
    {
        /// <summary>
        /// Methode die het kwadraat van een ingevoerd getal berekend
        /// </summary>
        static double BerekenKwadraat(double ingevoerdGetal)
        {
            ingevoerdGetal = Math.Pow(ingevoerdGetal, 2);
            return ingevoerdGetal;
        }
        static void Main(string[] args)
        {
            Console.Write("Geef het getal waarvan je het kwadraat wil berekenen in: ");
            double getal = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine($"Het kwadraat van {getal} is {BerekenKwadraat(getal)}.");
            Console.ReadLine();
        }
    }
}
