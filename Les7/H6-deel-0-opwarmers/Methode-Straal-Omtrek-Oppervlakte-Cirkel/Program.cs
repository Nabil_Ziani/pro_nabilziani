﻿using System;

namespace Methode_Straal_Omtrek_Oppervlakte_Cirkel
{
    class Program
    {
        /// <summary>
        /// Methode die de straal van een cirkel berekent waarvan je de diameter meegeeft.
        /// </summary>
        static double BerekenStraal(double diameter)
        {
            double straal = diameter / 2;
            return straal;
        }
        /// <summary>
        /// Methode die de omtrek van een cirkel berekent.
        /// </summary>
        static double BerekenOmtrek(double straal)
        {

            double omtrek = 2 * Math.PI * straal;
            return Math.Round(omtrek, 2);
        }
        /// <summary>
        /// Methode die de oppervlakte van een cirkel berekent.
        /// </summary>
        static double BerekenOppervlakte(double straal)
        {
            double oppervlakte = Math.PI * Math.Pow(straal, 2);
            return Math.Round(oppervlakte, 2);
        }
        static void Main(string[] args)
        {
            Console.Write("Geef de diameter van de cirkel in: ");
            double ingegevenDiameter = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine($"De straal van een cirkel met diameter {ingegevenDiameter} = {BerekenStraal(ingegevenDiameter)}");

            // De methode BerekenOmtrek neemt als parameter de return-waarde van de methode BerekenStraal:
            Console.WriteLine($"Omtrek = {BerekenOmtrek(BerekenStraal(ingegevenDiameter))}");
            // De methode BerekenOppervlakte neemt als parameter de return-waarde van de methode BerekenStraal:
            Console.WriteLine($"Oppervlakte = {BerekenOppervlakte(BerekenStraal(ingegevenDiameter))}");
            Console.ReadLine();
        }
    }
}
