﻿using System;

namespace Methode_BerekenStraal
{
    class Program
    {
        /// <summary>
        /// Methode die de straal van een cirkel berekent waarvan je de diameter meegeeft.
        /// </summary>
        static double BerekenStraal(double diameter)
        {
            double straal = diameter / 2;
            return straal;
        }
        static void Main(string[] args)
        {
            Console.Write("Geef de diameter van de cirkel in: ");
            double ingegevenDiameter = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine($"De straal van een cirkel met diameter {ingegevenDiameter} = {BerekenStraal(ingegevenDiameter)}");
            Console.ReadLine();
        }
    }
}
