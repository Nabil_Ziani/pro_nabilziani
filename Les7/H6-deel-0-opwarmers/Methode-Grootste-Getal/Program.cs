﻿using System;

namespace Methode_Grootste_Getal
{
    class Program
    {
        static int BerekenGrootsteGetal(int getal1, int getal2)
        {
            int grootsteGetal = Math.Max(getal1, getal2);
            return grootsteGetal;
        }
        static void Main(string[] args)
        {
            Console.Write("Geef het eerste getal in: ");
            int eersteGetal = int.Parse(Console.ReadLine());
            Console.Write("Geef het tweede getal in: ");
            int tweedeGetal = int.Parse(Console.ReadLine());

            Console.WriteLine($"Het grootste getal is {BerekenGrootsteGetal(eersteGetal, tweedeGetal)}");
            Console.ReadLine();
        }
    }
}
