﻿using System;

namespace Methode_ToonEvenNummers
{
    class Program
    {
        /// <summary>
        /// Methode die alle oneven nummers toont van 1 tot een ingegeven getal
        /// </summary>
        static int ToonEvenNummers(int getal)
        {
            int eersteGetal = 1;
            while (eersteGetal <= getal)
            {
                Console.Write($"{eersteGetal} ");
                eersteGetal += 2;
            }
            return eersteGetal;
        }
        static void Main(string[] args)
        {
            Console.Write("Geef een getal in: ");
            int maxGetal = int.Parse(Console.ReadLine());
            ToonEvenNummers(maxGetal);
            Console.ReadLine();
        }
    }
}
