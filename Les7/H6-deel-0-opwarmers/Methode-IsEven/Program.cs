﻿using System;

namespace Methode_IsEven
{
    class Program
    {
        /// <summary>
        /// Methode die bepaald of een getal even is of oneven 
        /// </summary>
        static bool GetalIsEven(int getal)
        {
            bool evenGetal = getal % 2 == 0;
            return evenGetal;
        }
        static void Main(string[] args)
        {
            Console.Write("Geef een getal in: ");
            int ingegevenGetal = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine(GetalIsEven(ingegevenGetal));
            Console.ReadLine();
        }
    }
}
