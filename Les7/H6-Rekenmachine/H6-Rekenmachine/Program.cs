﻿using System;

namespace H6_Rekenmachine
{
    class Program
    {
        static void Main(string[] args)
        {
            double resultaat = 0; // hierin slagen we het resultaat op, zodat het ook buiten de functie kan gebruikt worden

            Console.Write("Geef het eerste getal in: ");
            double eersteGetal = Convert.ToDouble(Console.ReadLine());
            Console.Write("Geef het tweede getal in: ");
            double tweedeGetal = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Je kan volgende bewerkingen uitvoeren: optellen, aftrekken, vermenigvuldigen, delen, modulo en machtsverheffing.");
            Console.Write("Welke bewerking wil je uitvoeren: ");
            string berekening = Console.ReadLine();
            switch (berekening)
            {
                case "optellen":
                    resultaat = TelOp(eersteGetal, tweedeGetal);
                    break;
                case "aftrekken":
                    resultaat = TrekAf(eersteGetal, tweedeGetal);
                    break;
                case "vermenigvuldigen":
                    resultaat = Vermenigvuldig(eersteGetal, tweedeGetal);
                    break;
                case "delen":
                    resultaat = Deel(eersteGetal, tweedeGetal);
                    break;
                case "machtsverheffing":
                    resultaat = Machtsverheffing(eersteGetal, tweedeGetal);
                    break;
                case "modulo":
                    resultaat = Modulo(eersteGetal, tweedeGetal);
                    break;
                default:
                    Console.WriteLine("Je hebt niet het juiste teken ingegeven.");
                    break;
            }
            double nieuweResultaat = resultaat; // Deze variabele gaat éénmaal in de loop steeds nieuwe waarde krijgen

            // Indien user nog bewerkingen wilt uitvoeren gaan we in een loop
            Console.WriteLine("Nog een bewerking uitvoeren?");
            string optie = Console.ReadLine();

            if (optie != "nee")
            {
                while (optie != "nee")
                {
                    // We gaan verder bewerkingen uitvoeren en telkens vragen we opnieuw of de user nog één wilt uitvoeren, dit blijven we doen tot user "nee" ingeeft.
                    nieuweResultaat = ExtraBewerkingen(nieuweResultaat); /* We slaan de "nieuwe" resultaat telkens opnieuw op in de variabele "nieuweResultaat", 
                                                                            anders blijft het de waarde van de eerste bewerking behouden.*/
                    Console.WriteLine("Nog een bewerking uitvoeren?");
                    optie = Console.ReadLine();
                }
            }
            Console.ReadLine();
        }

        static double ExtraBewerkingen(double nieuweResultaat)
        {
            Console.Write("Welke bewerking wil je uitvoeren: ");
            string berekening = Console.ReadLine();
            Console.Write("Geef het tweede getal in: ");
            double getal = Convert.ToDouble(Console.ReadLine());
            switch (berekening)
            {
                case "optellen":
                    nieuweResultaat = TelOp(nieuweResultaat, getal);
                    break;
                case "aftrekken":
                    nieuweResultaat = TrekAf(nieuweResultaat, getal);
                    break;
                case "vermenigvuldigen":
                    nieuweResultaat = Vermenigvuldig(nieuweResultaat, getal);
                    break;
                case "delen":
                    nieuweResultaat = Deel(nieuweResultaat, getal);
                    break;
                case "machtsverheffing":
                    nieuweResultaat = Machtsverheffing(nieuweResultaat, getal);
                    break;
                case "modulo":
                    nieuweResultaat = Modulo(nieuweResultaat, getal);
                    break;
                default:
                    Console.WriteLine("Je hebt niet het juiste teken ingegeven.");
                    break;
            }
            return nieuweResultaat;
        }
        static double TelOp(double getal1, double getal2)
        {
            double resultaat = (getal1 + getal2);
            Console.WriteLine($"{getal1} + {getal2} = {resultaat}");
            return resultaat;
        }
        static double TrekAf(double getal1, double getal2)
        {
            double resultaat = (getal1 - getal2);
            Console.WriteLine($"{getal1} - {getal2} = {resultaat}");
            return resultaat;
        }
        static double Vermenigvuldig(double getal1, double getal2)
        {
            double resultaat = (getal1 * getal2);
            Console.WriteLine($"{getal1} * {getal2} = {resultaat}");
            return resultaat;
        }
        static double Deel(double getal1, double getal2)
        {
            double resultaat = (getal1 / getal2);
            Console.WriteLine($"{getal1} / {getal2} = {resultaat}");
            return resultaat;
        }
        static double Machtsverheffing(double grondtal, double exponent)
        {
            double resultaat = Math.Pow(grondtal, exponent);
            Console.WriteLine($"{grondtal} ^ {exponent} = {resultaat}");
            return resultaat;
        }
        static double Modulo(double getal1, double getal2)
        {
            double resultaat = getal1 % getal2;
            Console.WriteLine($"{getal1} % {getal2} = {resultaat}");
            return resultaat;
        }
    }
}
