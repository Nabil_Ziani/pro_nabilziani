﻿using System;

namespace H6_Module_1_test_herschrijven
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Voer een keuze in: ");
            int inputKeuze = int.Parse(Console.ReadLine());
            
            //Rekenmachine
            if (inputKeuze == 1)
            {
                Rekenmachine();
            }
            //Password tester
            else if (inputKeuze == 2)
            {
                PasswordTester();
            }
            //Recyclage --> orakeltje-deel2
            else if (inputKeuze == 3)
            {
                Orakeltje();
            }
            //Computersolver
            else if (inputKeuze == 4)
            {
                ComputerSolver();
            }
        }

        static void Rekenmachine()
        {
            Console.Write("Geef een eerste getal in: ");
            int getal1 = int.Parse(Console.ReadLine());
            Console.Write("Geef een tweede getal in: ");
            int getal2 = int.Parse(Console.ReadLine());
            Console.WriteLine("Kies één van volgende operators uit: +, -, *, /, %");
            string operatorKeuze = Console.ReadLine();
            switch (operatorKeuze)
            {
                case "+":
                    int som = (getal1 + getal2);
                    if (som < 0)
                    {
                        Console.BackgroundColor = ConsoleColor.DarkRed;
                        Console.WriteLine($"{getal1} + {getal2} = {som}");
                        Console.ResetColor();
                    }
                    else
                    {
                        Console.WriteLine($"{getal1} + {getal2} = {som}");
                    }
                    break;
                case "-":
                    int verschil = (getal1 - getal2);
                    if (verschil < 0)
                    {
                        Console.BackgroundColor = ConsoleColor.DarkRed;
                        Console.WriteLine($"{getal1} - {getal2} = {verschil}");
                        Console.ResetColor();
                    }
                    else
                    {
                        Console.WriteLine($"{getal1} - {getal2} = {verschil}");
                    }
                    break;
                case "*":
                    int product = (getal1 * getal2);
                    if (product < 0)
                    {
                        Console.BackgroundColor = ConsoleColor.DarkRed;
                        Console.WriteLine($"{getal1} * {getal2} = {product}");
                        Console.ResetColor();
                    }
                    else
                    {
                        Console.WriteLine($"{getal1} * {getal2} = {product}");
                    }
                    break;
                case "/":
                    int quotiënt = (getal1 / getal2);
                    if (quotiënt < 0)
                    {
                        Console.BackgroundColor = ConsoleColor.DarkRed;
                        Console.WriteLine($"{getal1} / {getal2} = {quotiënt}");
                        Console.ResetColor();
                    }
                    else
                    {
                        Console.WriteLine($"{getal1} / {getal2} = {quotiënt}");
                    }
                    break;
                case "%":
                    int rest = (getal1 % getal2);
                    if (rest < 0)
                    {
                        Console.BackgroundColor = ConsoleColor.DarkRed;
                        Console.WriteLine($"{getal1} % {getal2} = {rest}");
                        Console.ResetColor();
                    }
                    else
                    {
                        Console.WriteLine($"{getal1} % {getal2} = {rest}");
                    }
                    break;
            }
            Console.ReadLine();
        }
        static void PasswordTester()
        {
            Console.Write("Geef je wachtwoord in: ");
            string wachtwoord = Console.ReadLine();
            if (wachtwoord == "TrumpSux")
            {
                Console.BackgroundColor = ConsoleColor.DarkGreen;
                Console.WriteLine("Toegelaten");
                Console.ReadLine();
            }
            else
            {
                Console.BackgroundColor = ConsoleColor.DarkRed;
                Console.WriteLine("Verboden");
                Console.ReadLine();
            }
        }
        static void Orakeltje()
        {
            Random rnd = new Random();
            Console.WriteLine("Ben je een man of vrouw?");
            string geslacht = Console.ReadLine();
            Console.WriteLine("Hoe oud ben je?");
            int leeftijd = int.Parse(Console.ReadLine());
            int maximaleLeeftijdVrouw = 150;
            int maximaleLeeftijdMan = 120;
            if (geslacht == "vrouw")
            {
                int aantalJaarMogelijk = maximaleLeeftijdVrouw - leeftijd;
                int aantalJaar = rnd.Next(5, aantalJaarMogelijk);
                Console.WriteLine($"Je zal nog {aantalJaar} jaar leven.");
            }
            else
            {
                int aantalJaarMogelijk = maximaleLeeftijdMan - leeftijd;
                int aantalJaar = rnd.Next(5, aantalJaarMogelijk);
                Console.WriteLine($"Je zal nog {aantalJaar} jaar leven.");
            }
            Console.ReadLine();
        }
        static void ComputerSolver()
        {
            Console.WriteLine("Does the computer turn on?");
            string antwoord1 = Console.ReadLine();
            if (antwoord1 == "yes")
            {
                Console.WriteLine("Are there any error messages?");
                string antwoord2 = Console.ReadLine();
                if (antwoord2 == "yes")
                {
                    Console.WriteLine("Perform a search for the error message");
                }
                else
                {
                    Console.WriteLine("Computer is fine");
                }
            }
            else
            {
                Console.WriteLine("Is the computer power light on?");
                string antwoord2 = Console.ReadLine();
                if (antwoord2 == "yes")
                {
                    Console.WriteLine("Turn the computer monitor on");
                }
                else
                {
                    Console.WriteLine("Check the computer power cord");
                }
            }
            Console.ReadLine();
        }
    }
}
