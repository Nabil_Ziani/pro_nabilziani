﻿using System;

namespace WerkenMetString
{
    class Program
    {
        static void Main(string[] args)
        {
            // Variabelen declareren en waarde toekennen
            string familienaam = "Ziani";
            string voornaam = "Nabil";
            string lievelingsmuziek = "Rap";

            // de info tonen in console
            Console.WriteLine($"Ik ben {familienaam} {voornaam} en mijn lievelingsmuziek is {lievelingsmuziek}");
            // Aantal letters naam en voornaam tonen
            Console.WriteLine($"Aantal letters familienaam: {familienaam.Length}");
            Console.WriteLine($"Aantal letters voornaam: {voornaam.Length}");
            // Eerste karakter van familienaam tonen
            Console.WriteLine($"Eerste karakter in familienaam: {familienaam.Substring(0, 1)}");
            // Tweede karakter van familienaam tonen
            Console.WriteLine($"Tweede karakter in familienaam: {familienaam.Substring(1, 1)}");
            // Eerste karakter van voornaam tonen
            Console.WriteLine($"Eerste karakter in voornaam: {voornaam.Substring(0, 1)}");
            // Tweede en derde karakter van familienaam tonen
            Console.WriteLine($"Tweede en derde karakter in familienaam: {familienaam.Substring(1, 2)}");
            // Laatste karakter van familienaam tonen
            int indexLaatsteKarakter = (familienaam.Length - 1);
            Console.WriteLine($"Laatste karakter in familienaam: {familienaam.Substring(indexLaatsteKarakter, 1)}");
            // Initialen tonen
            Console.WriteLine($"De initialen zijn: {familienaam.Substring(0, 1)}. {voornaam.Substring(0, 1)}.");
            // Familienaam spellen --> Karakters één per één tonen
            string familienaamUpper = familienaam.ToUpper(); 
            char[] karaktersArray = familienaamUpper.ToCharArray();
            Console.Write("Familienaam gespeld: ");
            for (int i = 0; i < karaktersArray.Length; i++)
            {
                if (i == indexLaatsteKarakter)  // Bij het laatste karakter wil ik geen streepje
                {
                    Console.Write($"{karaktersArray[i]}");
                } else
                {
                    Console.Write($"{karaktersArray[i]}-");
                }
            }
            Console.Write("\n");
            // Familienaam in de omgekeerde volgorde tonen
            Console.Write("Familienaam in omgekeerde volgorde: ");
            for (int i = indexLaatsteKarakter; i >= 0; i--)
            {
                Console.Write($"{karaktersArray[i]}");
            }
            Console.ReadLine();
        }
    }
}
