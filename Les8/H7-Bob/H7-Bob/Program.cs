﻿using System;

namespace H7_Bob
{
    class Program
    {
        static void Main(string[] args)
        {
            // NOG NIET KLAAR!!!

            // Array aanmaken waarin we de antwoorden opslaan
            string[] antwoorden = new string[5];

            Console.WriteLine("Je kan 5x iets schrijven naar Bob");
            for (int i = 0; i < antwoorden.Length; i++)
            {
                Console.WriteLine("Vraag: ");
                string antwoord = Console.ReadLine();
                char[] antwoordLaatsteTekens = new char[2];
                // De laatste 2 tekens opslaan in een array
                antwoord.CopyTo((antwoord.Length - 2), antwoordLaatsteTekens, 0, 2);

                if (antwoord.Contains("?"))
                {
                    Console.WriteLine("Sure.");
                }
                else if (antwoord.Contains("!"))
                {
                    Console.WriteLine("Whoa, chill out!");
                }
                else if (antwoord.Contains("?!"))
                {
                    Console.WriteLine("Calm down, I know what I'm doing!");
                }
                else if (antwoord.Contains(".."))
                {
                    Console.WriteLine("Fine. Be that way!");
                }
                else
                {
                    Console.WriteLine("Whatever.");
                }
            }
            Console.ReadLine();
        }
    }
}
