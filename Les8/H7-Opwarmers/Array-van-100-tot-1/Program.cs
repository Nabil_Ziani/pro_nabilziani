﻿using System;

namespace Array_van_100_tot_1
{
    class Program
    {
        static void Main(string[] args)
        {
            //Maak een array gevuld met de getallen van 100 tot 1
            int[] getallen = new int[100];
            int teller = 1;
            for (int i = 0; i < getallen.Length; i++)   //Eerst array vullen met getallen van 1 tot 100
            {
                getallen[i] = teller;
                teller++;
            }
            Array.Reverse(getallen);    //Dan array omdraaien
            int index = 0;
            while (index < getallen.Length)
            {
                Console.Write($"{getallen[index]}, ");
                index++;
            }
            Console.ReadLine();
        }
    }
}
