﻿using System;

namespace Array_van_0_tot_10
{
    class Program
    {
        static void Main(string[] args)
        {
            //Maak een array gevuld met de getallen van 0 tot 10
            int[] getallen = new int[11];   // We beginnen van 0 dus de array heeft een lengte van 11 getallen
            for (int i = 0; i < getallen.Length; i++)
            {
                getallen[i] = i;
                Console.Write($"{getallen[i]}, ");
            }
            Console.ReadLine();
        }
    }
}
