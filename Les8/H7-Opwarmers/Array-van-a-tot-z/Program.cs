﻿using System;

namespace Array_van_a_tot_z
{
    class Program
    {
        static void Main(string[] args)
        {
            //Maak een array gevuld met de letters a tot z
            char[] alfabet = "abcdefghijklmnopqrstuvwxyz".ToCharArray();
            for (int i = 0; i < alfabet.Length; i++)
            {
                Console.Write($"{alfabet[i]}, ");
            }
            Console.ReadLine();
        }
    }
}
