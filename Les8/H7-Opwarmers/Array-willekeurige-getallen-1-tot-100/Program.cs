﻿using System;

namespace Array_willekeurige_getallen_1_tot_100
{
    class Program
    {
        static void Main(string[] args)
        {
            //Maak een array gevuld met willekeurige getallen tussen 1 en 100 (array is 20 lang)
            int[] getallen = new int[20];
            Random rnd = new Random();
            for (int i = 0; i < getallen.Length; i++)
            {
                getallen[i] = rnd.Next(1, 101);
                Console.Write($"{getallen[i]}, ");
            }
            Console.ReadLine();
        }
    }
}
