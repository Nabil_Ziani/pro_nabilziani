﻿using System;

namespace Array_true_en_false
{
    class Program
    {
        static void Main(string[] args)
        {
            //Maak een array gevuld met afwisselend true en false (lengte is 30)
            bool[] tekst = new bool[30];
            for (int i = 0; i < tekst.Length; i++)
            {
                if (i % 2 == 0)
                {
                    tekst[i] = true;
                }
                else
                {
                    tekst[i] = false;
                }
                Console.Write($"{tekst[i]}, ");
            }
            Console.ReadLine();
        }
    }
}
