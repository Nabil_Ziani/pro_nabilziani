﻿using System;

namespace H7_Ondernemingsnummer
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Geef de ondernemingsnummer in: ");
            string ondernemingsnummer = Console.ReadLine().ToUpper();
            ControleOndernemingsnummer(ondernemingsnummer);
        }
        static void ControleOndernemingsnummer(string ondernemingsnummer)
        {
            bool nummerIsCorrect = true;

            // Ik ga om te beginnen de 4 eerste checken
            char[] eersteDeelCorrect = { 'B', 'E', ' ', '0' };
            for (int i = 0; i < 4; i++)
            {
                if (ondernemingsnummer[i] != eersteDeelCorrect[i])
                {
                    nummerIsCorrect = false;
                }
            }
            // Nu dat in orde is ga ik de getallen erna checken
            string hetGetal = "";
            string laatsteTweeCijfers = "";
            // De 7 getallen na de eerste 4 verkrijgen en in een variabele hetGetal steken
            for (int i = 4; i < ondernemingsnummer.Length; i++)
            {
                if (i < (ondernemingsnummer.Length) - 2)  
                {
                    hetGetal += ondernemingsnummer[i];
                }
                // De laatste 2 getallen verkrijgen en in variabele laatsteTweeCijfers steken
                else
                {
                    laatsteTweeCijfers += ondernemingsnummer[i];
                }
            }
            // Deling: --> De 2 laatste cijfers van hetGetal moeten gelijk zijn aan (97 - de rest van de gehele deling)
            int rest = int.Parse(hetGetal) % 97;
            if (int.Parse(laatsteTweeCijfers) != 97 - rest)
            {
                nummerIsCorrect = false;
            }
            // Het resultaat op scherm tonen
            Console.WriteLine($"De 7 getallen zijn: {hetGetal}");
            Console.WriteLine($"De 2 laatste cijfers zijn: {laatsteTweeCijfers}");
            Console.WriteLine($"De rest is {rest}, dit afgetrokken door 97 is {97 - rest}");
            if (nummerIsCorrect == true)
            {
                Console.WriteLine("Geldig ondernemingsnummer");
            }
            else
            {
                Console.WriteLine("Ongeldig ondernemingsnummer");
            }
            Console.ReadLine();
        }
    }
}
