﻿using System;

namespace H7_Pro_Galgje
{
    class Program
    {
        static void Main(string[] args)
        {
            string hetWoord = "artesis";
            for (int i = 0; i < hetWoord.Length; i++)
            {
                Console.Write("*");
            }

            Galgje(hetWoord);
        }

        /// <summary>
        /// Het spel galgje
        /// </summary>
        /// <param name="teRadenWoord">Geef hier het woord in dat geraden moet worden</param>
        static void Galgje(string teRadenWoord)
        {
            char[] teRadenWoordArray = teRadenWoord.ToCharArray();
            // Ik ga een lege array aanmaken die ik zal vullen met de waardes die ingegeven worden op de juiste plaats
            string[] legeArray = new string[teRadenWoord.Length];

            // De gebruiker gaat 10 pogingen kunnen maken, daarna is de galg opgebouwd en spel afgelopen
            int teller = 0; // Hierin bewaar ik het aantal pogingen
            string userInput; // Ik declareer dit buiten de loop zodat ik het kan gebruiken in mijn while-voorwaarde (scope)
            do
            {
                Console.Write("\n");    // gewoon een witlijn..
                Console.WriteLine("Geef letter in, of typ het volledige woord indien je het denkt te weten");
                userInput = Console.ReadLine();

                teller++;
                // Nu gaan we testen of het ingegeven karakter in ons woord zit:
                for (int j = 0; j < teRadenWoord.Length; j++)
                {
                    if (userInput == Convert.ToString(teRadenWoordArray[j]))
                    {
                        legeArray[j] = Convert.ToString(teRadenWoordArray[j]);
                    }
                }

                // Als de array gelijk is aan het te raden woord komen we hierin:
                if (userInput == teRadenWoord)
                {
                    Console.WriteLine("-".PadLeft(50, '-'));
                    Console.WriteLine("Correct! U hebt het juiste woord gevonden.");
                    Console.WriteLine($"Benodigde pogingen: {teller}");
                    break; // Uit de loop gaan, wanneer het volledige woord is geraden
                }

                // Nu wil ik telkens alle gegevens in legeArray tonen, dus een nieuwe for-loop
                for (int k = 0; k < legeArray.Length; k++)
                {
                    if (legeArray[k] == Convert.ToString(teRadenWoordArray[k]))
                    {
                        Console.Write(legeArray[k]);
                    }
                    else
                    {
                        Console.Write("*");
                    }
                }
            } while (teller < 10 && userInput != teRadenWoord);

            // Als gebruiker 10 pogingen heeft gedaan en het te raden woord niet heeft gevonden, komen we uit loop en voeren we dit uit:
            if (Convert.ToString(userInput) != teRadenWoord)
            {
                Console.Write("\n");
                Console.WriteLine("-".PadLeft(50, '-'));
                Console.WriteLine("Je hebt het woord niet kunnen raden, volgende keer beter!");
            }
            Console.ReadLine();
        }
    }
}
