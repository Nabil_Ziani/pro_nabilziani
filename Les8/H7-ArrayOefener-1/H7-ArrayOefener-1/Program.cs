﻿using System;

namespace H7_ArrayOefener_1
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] getallen = new int[10];
            Console.WriteLine("Voer 10 gehele getallen in");
            for (int i = 0; i < getallen.Length; i++)
            {
                getallen[i] = int.Parse(Console.ReadLine());
            }
            Console.WriteLine("*************************");
            Console.WriteLine($"Som is {Som(getallen)}, Gemiddelde is {Gemiddelde(getallen)}, Grootste getal is {GrootsteGetal(getallen)}");
            Console.WriteLine("*************************");
            Console.Write("Geef een minimum getal in: ");
            int minimumGetal = int.Parse(Console.ReadLine());
            GetallenBovenMinimum(getallen, minimumGetal);   //Functie die de getallen boven het minimumgetal weergeeft
            Console.ReadLine();
        }

        static int Som(int[] getallen)
        {
            int som = 0;
            for (int i = 0; i < getallen.Length; i++)
            {
                som += getallen[i];
            }
            return som;
        }
        static double Gemiddelde(int[] getallen)
        {
            double som = 0;
            double gemiddelde;
            for (int i = 0; i < getallen.Length; i++)
            {
                som += getallen[i];
            }
            gemiddelde = (som / getallen.Length);
            return gemiddelde;
        }
        static int GrootsteGetal(int[] getallen)
        {
            int grootsteGetal = 0;
            int vergelijkingsGetal = 0;
            for (int i = 0; i < getallen.Length; i++)
            {
                grootsteGetal = Math.Max(getallen[i], vergelijkingsGetal);
            }
            return grootsteGetal;
        }
        static void GetallenBovenMinimum(int[] getallen, int minimumGetal)
        {
            bool getallenBovenMinimum = false;  //Default staat de bool op false
            for (int i = 0; i < getallen.Length; i++)
            {
                if (getallen[i] >= minimumGetal)    // Als er één getal groter is dan het minimumGetal komen we in de if                                                    
                {                                   // --> het getal wordt dan geprint en de bool wordt gezet op true
                    Console.Write($"{getallen[i]}, ");
                    getallenBovenMinimum = true;
                }
            }
            if (getallenBovenMinimum == false)  // Als er geen enkele getal groter is dan het minimumGetal, blijft de bool false en printen we dit uit:
            {
                Console.WriteLine("Niets is groter");
            }
        }
    }
}
