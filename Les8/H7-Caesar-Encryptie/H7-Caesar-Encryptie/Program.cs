﻿using System;

namespace H7_Caesar_Encryptie
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Voer de tekst die je wil encrypteren in: ");
            string tekstEncrypteren = Console.ReadLine();
            tekstEncrypteren = tekstEncrypteren.ToUpper();
            char[] tekstArray1 = tekstEncrypteren.ToCharArray();
            EncrypteerTekst(tekstArray1, 3);

            Console.WriteLine("Voer de tekst die je wil decrypteren in: ");
            string tekstDecrypteren = Console.ReadLine();
            tekstDecrypteren = tekstDecrypteren.ToUpper();
            char[] tekstArray2 = tekstDecrypteren.ToCharArray();
            DecrypteerTekst(tekstArray2, 3);
        }

        static void EncrypteerTekst(char[] ingegevenArray, int getal)
        {
            char[] alfabet = { 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', };
            char[] nieuweArray = new char[alfabet.Length];
            int teller = 0; // Deze teller heb ik nodig om de eerste letters achteraan te zetten
            char[] nieuweIngegevenArray = new char[ingegevenArray.Length];

            for (int i = 0; i < alfabet.Length; i++) // We blijven evenlang loopen als de lengte van het gegeven array
            {
                if (i < alfabet.Length - getal)  // De lengte - getal (= de key) omdat die laatste plaatsen vrij moeten blijven
                {
                    nieuweArray[i] = alfabet[i + getal]; /* In de nieuwe array krijgt de letter met huidige index telkens de waarde van de letter x-aantal keer verschoven
                                                                   in de oude array, dus nieuweArray[0] krijgt de waarde van ingegevenArray[0 + getal] */
                }
                else
                {
                    nieuweArray[i] = alfabet[teller]; // Hier vullen we het laatste deel van de nieuwe array met het eerste deel van de oude array
                    teller++;
                }
            }
            teller = 0;
            /* Ik heb op dit moment 2 arrays die ik ga gebruiken, [alfabet] waarin het volledige alfabet zit en [nieuweArray] waarin de alfabet x-aantal keer verschoven is naar links
               naargelang de key die is opgegeven. Nu ga ik elke keer het volledige alfabet afgaan en telkens wanneer [ingegevenArray] correspondeert met de [alfabet] ga ik 
               een nieuwe array vullen met de waarde van [nieuweArray] (= de array die verschoven is), zo krijg ik de encryptie-letter van dat letter! Dit herhaal ik evenlang 
               als de lengte van het ingegeven woord [ingegevenArray]*/
            for (int j = 0; j < ingegevenArray.Length; j++)
            {
                for (int k = 0; k < alfabet.Length; k++)
                {
                    if (ingegevenArray[teller] == alfabet[k])
                    {
                        nieuweIngegevenArray[teller] = nieuweArray[k];
                    }
                }
                teller++;
            }
            // Nu tonen we de geëncrypteerde tekst
            for (int i = 0; i < nieuweIngegevenArray.Length; i++)
            {
                Console.Write(nieuweIngegevenArray[i]);
            }
            Console.ReadLine();
        }
        static void DecrypteerTekst(char[] ingegevenArray, int getal)
        {
            //De code is zo goed als hetzelfde als de vorige functie, dus voor uitleg zie daar..
            char[] alfabet = { 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', };
            char[] nieuweArray = new char[alfabet.Length];
            int teller = 0; 
            char[] nieuweIngegevenArray = new char[ingegevenArray.Length];

            for (int i = 0; i < alfabet.Length; i++)
            {
                if (i < alfabet.Length - getal) 
                {
                    nieuweArray[i] = alfabet[i + getal]; 
                }
                else
                {
                    nieuweArray[i] = alfabet[teller]; 
                    teller++;
                }
            }
            teller = 0;
            /* De enige aanpassingen voer ik hieronder uit, ik loop weer telkens over het hele alfabet en wanneer de letter van het ingegeven woord gelijk is aan 
               nieuweArray (de verschoven array) zet ik het gelijk aan de letter die correspondeert met de letter in het juiste alfabet. Zo krijg ik de originele tekst terug */
            for (int j = 0; j < ingegevenArray.Length; j++)
            {
                for (int k = 0; k < alfabet.Length; k++)
                {
                    if (ingegevenArray[teller] == nieuweArray[k])
                    {
                        nieuweIngegevenArray[teller] = alfabet[k];
                    }
                }
                teller++;
            }
            // Nu tonen we de originele tekst terug
            for (int i = 0; i < nieuweIngegevenArray.Length; i++)
            {
                Console.Write(nieuweIngegevenArray[i]);
            }
            Console.ReadLine();
        }
    }
}
