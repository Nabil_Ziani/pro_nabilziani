﻿using System;

namespace H7_Pro_Grote_Som
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine($"De som is {GroteSom(1, 3, 5, 10)}");
            // Je kan ook een array van 50 ints als parameter meegeven
            int[] getallen = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25 };
            Console.WriteLine($"De som van de 50 getallen is {GroteSom(getallen)}");
            Console.ReadLine();
        }

        static int GroteSom(params int[] getallen)
        {
            int som = 0;
            for (int i = 0; i < getallen.Length; i++)
            {
                som += getallen[i];
            }
            return som;
        }
    }
}
