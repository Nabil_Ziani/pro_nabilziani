﻿ using System;

namespace H7_LeveringsBedrijf
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Geef het gewicht van de pakket in: ");
            int gewicht = int.Parse(Console.ReadLine());
            Console.Write("Naar welke postcode wenst u dit pakket te versturen: ");
            string postcode = Console.ReadLine();
            int prijs = PrijsBerekenen(postcode, gewicht);
            if (prijs == 0) // Als de methode 0 returned wil dit zeggen dat er geen geldige postcode is meegegeven
            {
                Console.WriteLine("We leveren niet naar het gegeven postcode.");
            }
            else
            {
                Console.WriteLine($"Dit zal {prijs} euro kosten.");
            }
            Console.ReadLine();
        }
        
        /// <summary>
        /// Deze functie geeft de prijs weer voor een bepaalde postcode 
        /// </summary>
        static int PrijsBerekenen(string postcode, int gewicht)
        {
            string[] adressen = { "2020", "2050", "2060", "2100", "2140", "2160", "2170", "2180", "2550", "2600" }; // Array moet gesorteerd zijn
            int[] prijzen = { 214, 56, 62, 78, 44, 25, 36, 45, 88, 15 };
            int prijs = 0;

            for (int i = 0; i < adressen.Length; i++)
            {
                // Beide loops hebben 10 items, prijzen[i] is de prijs voor adressen[i]
                if (i == Array.BinarySearch(adressen, postcode))    // Als [i] gelijk is aan de index van de ingegeven postcode dan komen we in de if-statement
                {
                    prijs = prijzen[i] * gewicht;   // prijs is dan de prijs met zelfde index als adres * het gewicht
                }
            }
            return prijs;
        }
    }
}
