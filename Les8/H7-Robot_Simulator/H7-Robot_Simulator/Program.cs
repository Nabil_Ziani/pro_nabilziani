﻿using System;

namespace H7_Robot_Simulator
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Geef het eerste coördinaat in: ");
            int breedtegraad = int.Parse(Console.ReadLine());
            Console.Write("Geef de tweede coördinaat in: ");
            int lengtegraad = int.Parse(Console.ReadLine());
            // De gebruiker gaat 5 instructies ingeven en deze bewaren we in een array
            string[] instructies = new string[5];
            Console.WriteLine("Geef 5 instructies in (engels): ");
            for (int i = 0; i < instructies.Length; i++)
            {
                instructies[i] = Console.ReadLine();
            }
            Console.WriteLine("--".PadLeft(50, '-'));
            // Beginpositie:
            Console.SetCursorPosition(breedtegraad, lengtegraad);

            // Nu gaan we het gevolgde pad tekenen, we beginnen gericht naar het noorden
            bool bochtGenomen = false;
            int bochtRechts = 0;
            int bochtLinks = 0;
            for (int i = 0; i < instructies.Length; i++)
            {
                switch (instructies[i])
                {
                case "advance":
                    if (bochtGenomen == false || bochtLinks == 2 || bochtRechts == 2)
                    {
                        Console.Write("║");
                        Console.SetCursorPosition(breedtegraad, lengtegraad -= 1);
                    }
                    else
                    {
                        Console.Write("═");
                        if (bochtRechts > 0)
                        {
                            Console.SetCursorPosition(breedtegraad += 1, lengtegraad);
                        }
                        else
                        {
                            Console.SetCursorPosition(breedtegraad -= 1, lengtegraad);
                        }
                    }
                        break;
                case "turn right":
                    bochtGenomen = true;
                    if (bochtRechts == 0)
                    {
                        Console.Write("╔");
                        Console.SetCursorPosition(breedtegraad += 1, lengtegraad);
                    }
                    else if (bochtRechts == 1)
                    {
                        Console.Write("╗");
                        Console.SetCursorPosition(breedtegraad, lengtegraad += 1);
                    }
                    else
                    {
                        Console.Write("╝");
                        Console.SetCursorPosition(breedtegraad, lengtegraad += 1);
                    }
                    bochtRechts++;   // Dit gebruik ik om te weten dat er een bocht is genomen en hoeveel
                    break;
                case "turn left":
                    bochtGenomen = true;
                    if (bochtLinks == 0 && bochtGenomen == false)
                    {
                        Console.Write("╗");
                        Console.SetCursorPosition(breedtegraad -= 1, lengtegraad);
                    }
                    else if (bochtLinks == 1)
                    {
                        Console.Write("╔");
                        Console.SetCursorPosition(breedtegraad, lengtegraad += 1);
                    }
                    else
                    {
                        Console.Write("╚");
                        Console.SetCursorPosition(breedtegraad, lengtegraad += 1);
                    }
                    bochtLinks++;   // Dit gebruik ik om te weten dat er een bocht is genomen en hoeveel
                    break;
                default:
                    break;
                }
            }
            Console.ReadLine();
        }
    }
}
