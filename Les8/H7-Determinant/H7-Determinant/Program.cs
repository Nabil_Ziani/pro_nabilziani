﻿using System;

namespace H7_Determinant
{
    class Program
    {
        static void Main(string[] args)
        {
            int[,] aMatrix =
            {
              {2,4},
              {3,5}
            };
            int[,] bMatrix =
            {
                { 1, 6, 4 },
                { 2, 7, 3},
                { 8, 9, 5}
            };

            Console.WriteLine($"Determinant van matrix is {BerekenDeterminant(aMatrix)}");
            Console.WriteLine($"Determinant van matrix is {BerekenDeterminant(bMatrix)}");
            Console.ReadLine();
        }
        static int BerekenDeterminant(int[,] matrix)
        {
            int determinant = 0;
            if (matrix.GetLength(1) == 2)
            {
                int getalA = matrix[0, 0];
                int getalB = matrix[0, 1];
                int getalC = matrix[1, 0];
                int getalD = matrix[1, 1];

                determinant = (getalA * getalD) - (getalB * getalC);
            }
            else if (matrix.GetLength(1) == 3)
            {
                int getalA = matrix[0, 0];
                int getalB = matrix[0, 1];
                int getalC = matrix[0, 2];
                int getalD = matrix[1, 0];
                int getalE = matrix[1, 1];
                int getalF = matrix[1, 2];
                int getalG = matrix[2, 0];
                int getalH = matrix[2, 1];
                int getalI = matrix[2, 2];

                // Berekening op kladblok gedaan
                determinant = getalA * (getalE * getalI) - (getalF * getalH) - getalB * (getalD * getalI) - (getalF * getalG) + getalC * (getalD * getalH) - (getalE * getalG);
            }
            return determinant;
        }
    }
}
