﻿using System;

namespace H7_Voetbalcoach
{
    class Program
    {
        static void Main(string[] args)
        {
            string[,] spelers = new string[12, 2];  // 2-dimensionale array aanmaken waarin we de knappe/domme acties van een speler gaan bijhouden
            // Variabelen declareren en een default-waarde toekennen...
            string rugnummer = "", actie = "", aantalActies = "";  
            int aantalSpelers = 0;

            // Ik zorg ervoor dat de user uit de loop kan komen zodra hij eender waar "99" ingeeft:  
            while (rugnummer != "99" && actie != "99" && aantalActies != "99")
            {
                if (rugnummer != "99" && actie != "99" && aantalActies != "99")
                {
                    Console.Write("Geef het rugnummer van de speler in: ");
                    rugnummer = Console.ReadLine();
                }
                if (rugnummer != "99" && actie != "99" && aantalActies != "99")
                {
                    Console.Write("Wil je een goede actie (a) of domme actie (b) ingeven: ");
                    actie = Console.ReadLine();
                }
                if (rugnummer != "99" && actie != "99" && aantalActies != "99")
                {
                    Console.Write("Geef het aantal acties in: ");
                    aantalActies = Console.ReadLine();
                }
                if (rugnummer != "99" && actie != "99" && aantalActies != "99")
                {
                    int x = 0;                                          // Het aantal spelers vasthouden in de variabele aantalSpelers. In de if-statement 
                    aantalSpelers = Math.Max(x, int.Parse(rugnummer));  // omdat ik niet wil dat de variabele de waarde 99 krijgt wanneer de user dit ingeeft
                }
                SpelersInfo(rugnummer, actie, aantalActies, spelers, aantalSpelers);   // Met deze functie vullen we de array met de acties van elk ingegeven speler
            }
            Console.ReadLine();
        }

        /// <summary>
        /// Deze functie gaat een ingegeven 2-dimensionale array vullen met de gewenste data
        /// </summary>
        /// <param name="rugnummer">De rugnummer van de speler die je in de array wilt steken</param>
        /// <param name="actie">De actie die je wil toevoegen in de array, knap (a) of dom (b)</param>
        /// <param name="aantalActies">Het aantal acties die je wil meegeven</param>
        /// <param name="spelers">De array die je wilt vullen</param>
        /// <param name="aantalSpelers">Het aantal spelers die in de array worden gestoken (de array is niet noodzakelijk volledig gevuld)</param>
        static void SpelersInfo(string rugnummer, string actie, string aantalActies, string[,] spelers, int aantalSpelers)
        {
            int index = int.Parse(rugnummer) - 1;  // De index van de speler bepalen

            if (actie == "a" && index <= 12 && int.Parse(rugnummer) <= 12)    // Ik moet erbij als voorwaarde geven "zolang index kleiner is dan 12", anders krijg ik error   
            {                                                                 // wanneer user 99 ingeeft, hetzelfde geldt voor rugnummer
                spelers[index , 0] = aantalActies;
            }
            else if (actie == "b" && index <= 12 && int.Parse(rugnummer) <= 12)
            {
                spelers[index, 1] = aantalActies;
            }
            else // We komen hierin zodra de user 99 ingeeft
            {
                Console.WriteLine("-".PadRight(50, '-'));   // Gwn een lijn trekken boven de finale statistieken 
                Console.WriteLine("Rugnummer\tGoede\tDomme\tVerschil");
                for (int i = 0; i < aantalSpelers; i++)
                {
                    int verschil = int.Parse(spelers[i, 0]) - int.Parse(spelers[i, 1]);
                    if (verschil < 0)
                    {
                        Console.WriteLine($"{i + 1}\t\t  {spelers[i, 0]}\t  {spelers[i, 1]}\t  {verschil}\t --> Minst performante speler");
                    }
                    else if (verschil > 0)
                    {
                        Console.WriteLine($"{i + 1}\t\t  {spelers[i, 0]}\t  {spelers[i, 1]}\t  {verschil}\t --> Meest performante speler");
                    }
                    else
                    {
                        Console.WriteLine($"{i + 1}\t\t  {spelers[i, 0]}\t  {spelers[i, 1]}\t  {verschil}\t --> Meest gemiddelde speler");
                    }
                }
                Console.WriteLine("-".PadRight(50, '-'));
            }
        }
    }
}
