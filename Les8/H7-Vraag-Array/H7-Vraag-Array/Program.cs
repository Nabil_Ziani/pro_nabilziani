﻿using System;

namespace H7_Vraag_Array
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] vragen = new string[5];
            vragen[0] = "Hoe oud ben je?";
            vragen[1] = "Wat is je postcode?";
            vragen[2] = "Hoeveel broers heb je?";
            vragen[3] = "Hoeveel zussen heb je?";
            vragen[4] = "Wanneer ben je geboren?";

            int[] antwoorden = new int[5];
            for (int i = 0; i < vragen.Length; i++)
            {
                Console.WriteLine(vragen[i]);
                antwoorden[i] = int.Parse(Console.ReadLine());
            }
            Console.WriteLine("Je antwoorden: ");
            for (int i = 0; i < antwoorden.Length; i++)
            {
                Console.WriteLine($"{vragen[i]}: {antwoorden[i]}");
            }
            Console.ReadLine();
        }
    }
}
