﻿using System;

namespace H7_Matrix_Multiplier
{
    class Program
    {
        static void Main(string[] args)
        {
            /* Ik heb 2, 2x2 matrixen */
            int[,] matrix1 = { { 1, 2 }, { 3, 4 } };
            int[,] matrix2 = { { 5, 6 }, { 7, 8 } };

            VermenigvuldigMatrix(matrix1, matrix2);
            Console.ReadLine();
        }
        static void VermenigvuldigMatrix(int[,] eersteMatrix, int[,] tweedeMatrix)
        {
            int[,] resultaatMatrix = new int[2, 2];

            // Nu vullen we de nieuwe matrix met de juiste waardes a.d.h.v. berekeningen
            resultaatMatrix[0, 0] = (eersteMatrix[0,0] * tweedeMatrix[0,0]) + (eersteMatrix[0,1] * tweedeMatrix[1,0]);  // Linksboven
            resultaatMatrix[0, 1] = (eersteMatrix[0,0] * tweedeMatrix[0,1]) + (eersteMatrix[0,1] * tweedeMatrix[1,1]);  // Rechtsboven
            resultaatMatrix[1, 0] = (eersteMatrix[1,0] * tweedeMatrix[0,0]) + (eersteMatrix[1,1] * tweedeMatrix[1,0]);  // Linksonder
            resultaatMatrix[1, 1] = (eersteMatrix[1,0] * tweedeMatrix[0,1]) + (eersteMatrix[1,1] * tweedeMatrix[1,1]);  // Rechtsonder

            Console.WriteLine("Het resultaat is: ");
            for (int i = 0; i < resultaatMatrix.GetLength(0); i++)
            {
                for (int j = 0; j < resultaatMatrix.GetLength(1); j++)
                {
                    Console.Write($"{resultaatMatrix[i, j]} ");
                }
                Console.WriteLine();
            }
        }
    }
}
