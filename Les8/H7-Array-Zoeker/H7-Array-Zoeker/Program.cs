﻿using System;

namespace H7_Array_Zoeker
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] getallen = new int[10];
            Console.WriteLine("Voer 10 gehele getallen in: ");
            for (int i = 0; i < 10; i++)
            {
                getallen[i] = int.Parse(Console.ReadLine());
            }
            Console.WriteLine("****************************");
            Console.WriteLine("Welke getal moet verwijderd worden?");
            int getalVerwijderen = int.Parse(Console.ReadLine());
            Array.Sort(getallen); // De array sorteren
            int indexGetalVerwijderen = Array.BinarySearch(getallen, getalVerwijderen); // De index van het getal dat verwijderd moet worden verkrijgen
            Array.Clear(getallen, indexGetalVerwijderen, 1); // Het getal verwijderen
            getallen[indexGetalVerwijderen] = -1; // De waarde -1 toekennen aan het verwijderde getal

            // Nu moeten de getallen vanaf het verwijderd getal een shift naar links krijgen en het verwijderd getal moet helemaal naar achter
            int[] nieuweArray = new int[Math.Abs(indexGetalVerwijderen - getallen.Length)];   // Nieuwe array aanmaken waarin enkel het deel vanaf het verwijderd getal zal worden opgeslagen
            Array.Copy(getallen, indexGetalVerwijderen, nieuweArray, 0, Math.Abs(indexGetalVerwijderen - getallen.Length)); // Juiste deel kopiëren naar de nieuwe array
            // De functie Shift neemt een array in en verschuift de array één stap naar links
            nieuweArray = Verschuiven(nieuweArray);
            Array.Copy(nieuweArray, 0, getallen, indexGetalVerwijderen, Math.Abs(indexGetalVerwijderen - getallen.Length)); // De nieuwe array kopiëren naar de juiste plaats in array [getallen]..

            Console.Write("Resultaat is: ");
            for (int i = 0; i < getallen.Length; i++)
            {
                Console.Write($"{getallen[i]}, ");
            }
            Console.ReadLine();
        }

        /// <summary>
        /// Deze functie neemt een array in en verplaatst alles één stap naar links
        /// </summary>
        public static int[] Verschuiven(int[] ingegevenArray)
        {
            int[] nieuweArray = new int[ingegevenArray.Length];
            for (int i = 0; i < ingegevenArray.Length; i++) // We blijven evenlang loopen als de lengte van het gegeven array
            {
                if (i < ingegevenArray.Length - 1)  // De lengte - 1 omdat de laatste plaats moet vrij blijven voor de uiterst linkse getal
                    nieuweArray[i] = ingegevenArray[i + 1]; /* In de nieuwe array krijgt het getal met huidige index telkens de waarde van het getal erna in de oude array
                                                               Dus nieuweArray[0] krijgt de waarde van ingegevenArray[1] */
                else
                    nieuweArray[i] = ingegevenArray[0]; // We komen in de else wanneer we op de laatste plaats staan, deze krijgt de waarde van het eerste getal in de oude array
            }
            return nieuweArray;
        }
    }
}
