﻿using System;

namespace H7_2D_Array_Viewer
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] array = { 15, 6, 9 };
            int[] array2 = { 0, 1, 2, 3, 4, 5, 6 };
            VisualiseerArray(array);
            VisualiseerArray(array2);

            int[,] dimensionalArray = { { 15, 6, 9 }, { 1, 2, 3 }, { 6, 9, 12 } };
            VisualiseerDimensioneleArray(dimensionalArray);
        }
        static void VisualiseerArray(int[] getallen)
        {
            for (int i = 0; i < getallen.Length; i++)
            {
                Console.Write($"{getallen[i]}\t");
            }
            Console.ReadLine();
        }
        static void VisualiseerDimensioneleArray(int[,] dimensionalArray)
        {
            //Om telkens op een nieuwe lijn te komen maak ik gebruik van nesting
            for (int i = 0; i < dimensionalArray.GetLength(0); i++)
            {
                for (int j = 0; j < dimensionalArray.GetLength(1); j++)
                {
                    Console.Write($"{dimensionalArray[i, j]}\t");
                }
                Console.WriteLine();
            }
            Console.ReadLine();
        }
    }
}
