﻿using System;

namespace H7_ParkeerGarage
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Geef een aantal auto's in: ");
            int ingegevenAantalAutos = int.Parse(Console.ReadLine());
            int teller = 1; // Hiermee geef ik de nummering van de auto's weer bij het vragen naar de parkeertijden
            double[] parkeerTijden = new double[ingegevenAantalAutos];
            for (int i = 0; i < ingegevenAantalAutos; i++)                
            {
                Console.WriteLine($"Geef parkeertijd auto {teller} in (uren): ");
                parkeerTijden[i] = double.Parse(Console.ReadLine());
                teller++;
            }

            // Resultaat in tabelvorm:
            double totaalDuur = 0;
            double totaalKost = 0;
            teller = 1; // teller resetten, terug op 1 zetten

            Console.WriteLine("Auto \t Duur \t Kost");
            for (int i = 0; i < ingegevenAantalAutos; i++)  // De loop zal 3x herhaald worden
            {
                Console.WriteLine($"{teller} \t {parkeerTijden[i]} \t {BerekenKosten(GeefTijdsDuur(parkeerTijden, i))}");
                totaalDuur += parkeerTijden[i];
                totaalKost += BerekenKosten(GeefTijdsDuur(parkeerTijden, i));
                teller++;   
            }
            Console.WriteLine($"Totaal:  {totaalDuur} \t {totaalKost}");
            Console.ReadLine();

        }

        /// <summary>
        /// Deze methode geeft de tijdsduur van een bepaalde auto terug
        /// </summary>
        /// <param name="parkeerTijden">Hier geef je de array in waarin de verschillende parkeertijden van de auto's zitten</param>
        /// <param name="auto">Hier geef je het getal in van de auto waarvan je de parkeertijd wilt hebben</param>
        /// <returns></returns>
        static double GeefTijdsDuur(double[] parkeerTijden, int auto)
        {
            double parkeerTijd = 0;
            for (int i = 0; i < parkeerTijden.Length; i++)
            {
                if (i == auto)  
                {
                    parkeerTijd = parkeerTijden[i]; // Om te zorgen dat we maar één keer loopen bij aanroep van de functie (enkel voor de gewenste auto)
                }                                   // plaats ik het in de if-statement
            }
            return parkeerTijd;
        }

        /// <summary>
        /// Deze methode neemt een tijdsduur in en returned de prijs 
        /// </summary>
        static double BerekenKosten(double tijdsduur)
        {
            double prijs = 2;
            if (tijdsduur <= 3)
            {
                prijs = 2;
            }
            else
            {
                for (int i = 0; i < (tijdsduur - 3); i++) // Voor elke uur na 3 uur voegen we 0.5 euro toe
                {
                    if (prijs < 10)    // Er wordt maximaal 10 euro aangerekend, zodra prijs 10 is gaan we uit loop 
                    {
                        prijs += 0.5;
                    }
                }
            }
            return prijs;
        }
    }
}
