﻿using System;

namespace H7_Hamming_Distance
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Geef 2 gelijke DNA-strings in: ");
            char[] eersteString = Console.ReadLine().ToCharArray();
            char[] tweedeString = Console.ReadLine().ToCharArray();
            for (int i = 0; i < eersteString.Length; i++)
            {
                if (eersteString[i] != tweedeString[i])
                {
                    Console.Write("^");
                }
                else
                {
                    Console.Write(" ");
                }
            }
            Console.ReadLine();
        }
    }
}
