﻿using System;

namespace H7_Array_Viewer
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] array = { 15, 6, 9 };
            int[] array2 = { 0, 1, 2, 3, 4, 5, 6 };
            VisualiseerArray(array);
            VisualiseerArray(array2);
        }
        static void VisualiseerArray(int[] getallen)
        {
            for (int i = 0; i < getallen.Length; i++)
            {
                Console.Write($"{getallen[i]}\t");
            }
            Console.ReadLine();
        }
    }
}
