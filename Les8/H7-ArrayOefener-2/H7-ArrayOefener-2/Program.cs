﻿using System;

namespace H7_ArrayOefener_2
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] arrayA = new int[5];
            int[] arrayB = new int[5];
            Console.WriteLine("Voer 5 gehele getallen in: ");
            for (int i = 0; i < arrayA.Length; i++)
            {
                arrayA[i] = int.Parse(Console.ReadLine());
            }
            Console.WriteLine("Voer 5 gehele getallen in: ");
            for (int i = 0; i < arrayB.Length; i++)
            {
                arrayB[i] = int.Parse(Console.ReadLine());
            }
            Console.Write($"Array C bevat: ");
            for (int i = 0; i < ArraySom(arrayA, arrayB).Length; i++)
            {
                Console.Write($"{ArraySom(arrayA, arrayB)[i]}, ");  // De functie ArraySom geeft de som van 2 gegeven arrays terug in een nieuwe array, in de loop 
            }                                                       // wordt de functie steeds herhaald met als index [i] om alle getallen in de array te tonen
            Console.ReadLine();
        }

        /// <summary>
        /// Geeft de som van 2 arrays in een nieuwe array terug
        /// </summary>
        static int[] ArraySom(int[] array1, int[] array2)
        {
            int som = 0;
            int[] arrayC = new int[5];
            for (int i = 0; i < 5; i++)
            {
                som = array1[i] + array2[i];
                arrayC[i] = som;
            }
            return arrayC;
        }
    }
}
