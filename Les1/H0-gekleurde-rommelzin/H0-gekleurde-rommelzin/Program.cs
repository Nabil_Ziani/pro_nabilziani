﻿using System;

namespace H0_gekleurde_rommelzin
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Dit is mijn eerste C#-programma");
            Console.WriteLine("*******************************");

            Console.ForegroundColor = ConsoleColor.DarkGreen;
            Console.WriteLine("Wat is je favoriete kleur?");
            Console.ResetColor();
            string inputFavorieteKleur = Console.ReadLine();

            Console.ForegroundColor = ConsoleColor.DarkRed;
            Console.WriteLine("Wat is je favoriete eten?");
            Console.ResetColor();
            string inputFavorieteEten = Console.ReadLine();

            Console.ForegroundColor = ConsoleColor.DarkYellow;
            Console.WriteLine("Wat is je favoriete auto?");
            Console.ResetColor();
            string inputFavorieteAuto = Console.ReadLine();

            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine("Wat is je favoriete film?");
            Console.ResetColor();
            string inputFavorieteFilm = Console.ReadLine();

            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.WriteLine("Wat is je favoriete boek?");
            Console.ResetColor();
            string inputFavorieteBoek = Console.ReadLine();

            Console.WriteLine("");
            Console.ForegroundColor = ConsoleColor.Red;
            Console.Write($"Je favoriete kleur is {inputFavorieteEten}. Je eet graag {inputFavorieteAuto}. Je favoriete auto is {inputFavorieteKleur}. ");
            Console.Write($"Je lievelingsfilm is {inputFavorieteBoek} en je favoriete boek is {inputFavorieteFilm}.");
            Console.ReadLine();
        }
    }
}
