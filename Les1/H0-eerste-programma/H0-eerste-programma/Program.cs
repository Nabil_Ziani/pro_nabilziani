﻿using System;

namespace H0_eerste_programma
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Dit is mijn eerste C#-programma");
            Console.WriteLine("*******************************");

            Console.Write("Typ je naam: ");
            string inputNaam = Console.ReadLine();
            Console.WriteLine($"Hallo, {inputNaam}!");
            Console.ReadLine();
        }
    }
}
