﻿using System;

namespace H0_rommelzin
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Dit is mijn eerste C#-programma");
            Console.WriteLine("*******************************");

            Console.WriteLine("Wat is je favoriete kleur?");
            string inputFavorieteKleur = Console.ReadLine();
            Console.WriteLine("Wat is je favoriete eten?");
            string inputFavorieteEten = Console.ReadLine();
            Console.WriteLine("Wat is je favoriete auto?");
            string inputFavorieteAuto = Console.ReadLine();
            Console.WriteLine("Wat is je favoriete film?");
            string inputFavorieteFilm = Console.ReadLine();
            Console.WriteLine("Wat is je favoriete boek?");
            string inputFavorieteBoek = Console.ReadLine();
            Console.WriteLine("");

            Console.Write($"Je favoriete kleur is {inputFavorieteEten}. Je eet graag {inputFavorieteAuto}. ");
            Console.Write($"Je lievelingsfilm is {inputFavorieteBoek} en je favoriete boek is {inputFavorieteFilm}.");
            Console.ReadLine();
        }
    }
}
