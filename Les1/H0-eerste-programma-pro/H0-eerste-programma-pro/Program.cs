﻿using System;

namespace H0_eerste_programma_pro
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Dit is mijn eerste C#-programma");
            Console.WriteLine("*******************************");

            Console.Write("Typ je voornaam: ");
            string inputVoornaam = Console.ReadLine();
            Console.Write("Typ je achternaam: ");
            string inputAchternaam = Console.ReadLine();
            Console.WriteLine("");
            Console.WriteLine($"Dus je naam is: {inputAchternaam} {inputVoornaam}");
            Console.WriteLine($"Of: {inputVoornaam} {inputAchternaam}");
            Console.ReadLine();
        }
    }
}
